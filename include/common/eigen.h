/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#pragma once

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/StdVector>

template<typename T>
using eigen_compatible_stdvector = std::vector<T, Eigen::aligned_allocator<T>>;

template<typename T>
using MatxCM = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor>;

template<typename T>
using MatxRM = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

using vec3d = Eigen::Matrix<double, 3, 1>;
using mat3d = Eigen::Matrix<double, 3, 3, Eigen::RowMajor>;
using vecxd = Eigen::Matrix<double, Eigen::Dynamic, 1>;
using matxd = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
using vec_mat3d = eigen_compatible_stdvector<mat3d>;
using vec_matxd = eigen_compatible_stdvector<matxd>;

