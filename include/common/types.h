/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#pragma once

#include <vector>
#include "common/point.hpp"

#ifdef __NVCC__
#define HIDE_THIS_FROM_NVCC
#endif

using vec_int = std::vector<int>;
using vec_size_t = std::vector<size_t>;
using vec_double = std::vector<double>;
using vec_point_3d = std::vector<point_3d>;
using vec_quadpt_3d = std::vector<quadrature_point_3d>;

struct entity_params
{
    int     dim;
    int     tag;
    int     etype;
    int     gorder;
    int     aorder;
};

constexpr size_t num_dofs_3D(size_t AK)
{
    return ((AK+3)*(AK+2)*(AK+1))/6;
}

constexpr size_t num_dofs_2D(size_t AK)
{
    return ((AK+2)*(AK+1))/2;
};
