/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#pragma once

#include "libgmshdg/silo_output.hpp"
#include "maxwell/maxwell_interface.h"

namespace maxwell {

/* Field must be either 'field' or 'pinned_field'. Too early to use a concept. */
template<typename State, typename Field>
void
eval_boundary_sources(const model& mod, const parameter_loader& mpl,
    State& state, Field& srcfield)
{
    auto& bds = mod.boundary_descriptors();
    assert( srcfield.num_dofs == mod.num_fluxes() );

    size_t face_num_base = 0;
    for (auto& e : mod)
    {
        auto etag = e.material_tag();
        for (size_t iF = 0; iF < e.num_faces(); iF++)
        {
            auto gfnum = face_num_base + iF;
            auto bd = bds[gfnum];
            if (bd.type == face_type::NONE or bd.type == face_type::INTERFACE
#ifdef USE_MPI
                    or bd.type == face_type::INTERPROCESS_BOUNDARY
#endif /* USE_MPI */
               )
                continue;

            auto& pf = e.face(iF);
            auto bar = pf.barycenter();
            auto eps = mpl.epsilon(etag, bar);
            auto mu  = mpl.mu(etag, bar);
            auto Z = std::sqrt(mu/eps);

            vec3d n = state.eds[e.number()].normals.row(iF);

            switch ( mpl.boundary_type(bd.material_tag()) )
            {
                case boundary_condition::UNSPECIFIED:
                    break;

                case boundary_condition::PEC:
                    break;

                case boundary_condition::PMC:
                    break;

                case boundary_condition::E_FIELD:
                    break;

                case boundary_condition::H_FIELD:
                    break;

                case boundary_condition::IMPEDANCE:
                    break;

                case boundary_condition::PLANE_WAVE_E: {
                    auto fE = [&](const point_3d& pt) -> vec3d {
                        return mpl.eval_boundary_source(bd.material_tag(), pt, state.curr_time);
                    };
                    auto fH = [&](const point_3d& pt) -> vec3d {
                        vec3d H = mpl.eval_boundary_source(bd.material_tag(), pt, state.curr_time);
                        return n.cross(H)/Z;
                    };
                    matxd prjE = e.project_on_face(iF, fE);
                    auto num_bf = prjE.rows();
                    for (size_t i = 0; i < num_bf; i++)
                    {
                        srcfield.Ex[gfnum*num_bf + i] = prjE(i,0);
                        srcfield.Ey[gfnum*num_bf + i] = prjE(i,1);
                        srcfield.Ez[gfnum*num_bf + i] = prjE(i,2);
                    }
                    matxd prjH = e.project_on_face(iF, fH);
                    for (size_t i = 0; i < num_bf; i++)
                    {
                        srcfield.Hx[gfnum*num_bf + i] = prjH(i,0);
                        srcfield.Hy[gfnum*num_bf + i] = prjH(i,1);
                        srcfield.Hz[gfnum*num_bf + i] = prjH(i,2);
                    } 
                } break;

                case boundary_condition::PLANE_WAVE_H:
                    break;

                case boundary_condition::SURFACE_CURRENT:
                    break;
            }
        }

        face_num_base += e.num_faces();
    }
}

/* Field must be either 'field' or 'pinned_field'. Too early to use a concept. */
template<typename State, typename Field>
void
eval_interface_sources(const model& mod, const parameter_loader& mpl,
    State& state, Field& srcfield)
{
    auto& bds = mod.boundary_descriptors();
    assert( srcfield.num_dofs == mod.num_fluxes() );

    size_t face_num_base = 0;
    for (auto& e : mod)
    {
        for (size_t iF = 0; iF < e.num_faces(); iF++)
        {
            auto gfnum = face_num_base + iF;
            auto bd = bds[gfnum];
            if (bd.type == face_type::NONE or bd.type == face_type::BOUNDARY
#ifdef USE_MPI
                    or bd.type == face_type::INTERPROCESS_BOUNDARY
#endif /* USE_MPI */
               )
                continue;

            vec3d n = state.eds[e.number()].normals.row(iF);

            switch ( mpl.interface_type(bd.material_tag()) )
            {
                case interface_condition::UNSPECIFIED:
                    break;
                
                case interface_condition::E_FIELD: {
                    /* This is BS actually... */
                    auto fE = [&](const point_3d& pt) -> vec3d {
                        return mpl.eval_interface_source(bd.material_tag(), pt, state.curr_time);
                    };

                    matxd prjE = e.project_on_face(iF, fE);
                    auto num_bf = prjE.rows();
                    for (size_t i = 0; i < num_bf; i++)
                    {
                        srcfield.Ex[gfnum*num_bf + i] = prjE(i,0);
                        srcfield.Ey[gfnum*num_bf + i] = prjE(i,1);
                        srcfield.Ez[gfnum*num_bf + i] = prjE(i,2);
                    }
                } break;

                case interface_condition::SURFACE_CURRENT: {
                    auto fJ = [&](const point_3d& pt) -> vec3d {
                        vec3d J = mpl.eval_interface_source(bd.material_tag(), pt, state.curr_time);
                        return n.cross(J);
                    };

                    matxd prjJ = e.project_on_face(iF, fJ);
                    auto num_bf = prjJ.rows();
                    for (size_t i = 0; i < num_bf; i++)
                    {
                        srcfield.Hx[gfnum*num_bf + i] = prjJ(i,0);
                        srcfield.Hy[gfnum*num_bf + i] = prjJ(i,1);
                        srcfield.Hz[gfnum*num_bf + i] = prjJ(i,2);
                    } 
                } break;

                case interface_condition::H_FIELD:
                    break;
            }
        }

        face_num_base += e.num_faces();
    }
}

#ifdef USE_MPI
std::vector<double> compute_cell_ranks(const model&);
#endif /* USE_MPI */

using MaterialFunc = std::function<double(int, const point_3d&)>;

void
export_vector_field(const model&, silo&, const vecxd&, const vecxd&,
    const vecxd&, const std::string&, field_export_mode);

void
export_vector_field(const model&, silo&, const vecxd&, const vecxd&,
    const vecxd&, const MaterialFunc&, const std::string&,
    field_export_mode);

} // namespace maxwell

