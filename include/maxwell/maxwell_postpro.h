/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#pragma once

#include "maxwell/maxwell_interface.h"

namespace maxwell {

field_values eval_field(const field&, size_t, size_t, const vecxd&);
field_values compute_error(const model&, const solver_state&, const parameter_loader&);
field_values compute_energy(const model&, const solver_state&, const parameter_loader&);

void compare_at_gauss_points(const model&, const solver_state&, const parameter_loader&);

} // namespace maxwell

