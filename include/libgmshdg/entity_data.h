/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#pragma once

#include <numeric>
#include "common/eigen.h"
#include "common/types.h"

#ifdef ENABLE_GPU_SOLVER
#include "common/gpu_support.hpp"
#endif

#define NOT_PRESENT (~0ULL)

enum class entity_ordering {
    GMSH,
    BY_ORIENTATION
};

struct entity_data_cpu
{
    int                 g_order;            /* Geometric order */
    int                 a_order;            /* Approximation order */
    std::vector<size_t> num_elems;          /* No. of elements in the entity */
    size_t              num_orientations;   /* No. of bf. orientations */
    size_t              num_bf;             /* No. of dofs per elem */
    size_t              num_qp;             /* No. of integration pts per elem */
    size_t              num_fluxes;         /* No. of fluxes per elem */
    size_t              num_face_qp;        /* No. of integration pts per face */
    size_t              num_faces_per_elem; /* No. of faces */
    entity_ordering     ordering;           /* Entity element ordering */
    size_t              dof_base;           /* Global start position for dofs */
    size_t              flux_base;          /* Global start position for fluxes */

    std::vector<size_t> fluxdofs_mine;      /* Flux to DOF mapping, on myself */      
    std::vector<size_t> fluxdofs_neigh;     /* Flux to DOF mapping, on neighbours */

    /* Inverse of mass matrices. Populated only if geometric order > 1.
     * num_bf*num_elems rows, num_bf columns. Matrix at offset elem*num_bf
     * is the inverse mass matrix of element 'elem' */
    matxd               invmass_matrices;

    /* Differentiation matrices. Layout depends on geometric order.
     * Order 1: num_bf rows, 3*num_bf columns => three matrices
     * of size num_bf by num_bf. From L to R: M^{-1}Du, M^{-1}Dv, M^{-1}Dw.
     * Order > 1: num_qp*num_bf rows, 3*num_bf columns. From L to R:
     * Du, Dv, Dw for each quadrature point. Note that inverse mass is
     * not there! */
    matxd               differentiation_matrices;
    matxd               lifting_matrices;

    /* Jacobians. Layout depends on geometric order.
     * Order 1: 3*num_elems rows, 3 columns. Matrix at offset 3*elem
     * is the jacobian of element 'elem'. 
     * Order > 1: 3*num_elems*num_qp rows, 3 columns. Jacobians for
     * each quadrature point. */
    matxd               jacobians;

    matxd               normals;

    vecxd               cell_determinants;
    vecxd               face_determinants;
};

inline size_t
num_elems_all_orientations(const entity_data_cpu& ed)
{
    assert(ed.num_elems.size() == ed.num_orientations);
    return std::accumulate(ed.num_elems.begin(), ed.num_elems.end(), 0);
}

#ifdef ENABLE_GPU_SOLVER
struct entity_data_gpu
{
    int                         g_order;
    int                         a_order;
    size_t                      num_all_elems;
    device_vector<int32_t>      element_orientation;
    size_t                      num_bf;
    size_t                      num_fluxes;
    size_t                      num_faces_per_elem;

    size_t                      dof_base;
    size_t                      flux_base;


    texture_allocator<double>   differentiation_matrices;
    texture_allocator<double>   lifting_matrices;
    device_vector<double>       jacobians;
    device_vector<double>       normals;
    device_vector<double>       cell_determinants;
    device_vector<double>       face_determinants;


    device_vector<size_t>       fluxdofs_mine; 
    device_vector<size_t>       fluxdofs_neigh;

    entity_data_gpu();
    entity_data_gpu(entity_data_gpu&&) = default;
    entity_data_gpu(const entity_data_cpu&);
    ~entity_data_gpu();
};

#if 0
template<typename T>
void
gpu_copyin(const std::vector<T> src, T** dst)
{
    checkGPU( gpuMalloc( (void**)dst, src.size()*sizeof(T) ) );
    checkGPU( gpuMemcpy( *dst, src.data(), src.size()*sizeof(T), gpuMemcpyHostToDevice) );
}

template<typename T>
void
gpu_copyin(const MatxCM<T>& src, T** dst)
{
    checkGPU( gpuMalloc( (void**)dst, src.size()*sizeof(T) ) );
    checkGPU( gpuMemcpy( *dst, src.data(), src.size()*sizeof(T), gpuMemcpyHostToDevice) );
}

template<typename T>
void
gpu_copyin(const MatxRM<T>& src, T** dst)
{
    checkGPU( gpuMalloc( (void**)dst, src.size()*sizeof(T) ) );
    checkGPU( gpuMemcpy( *dst, src.data(), src.size()*sizeof(T), gpuMemcpyHostToDevice) );
}

template<typename T>
void
gpu_copyin(const Eigen::Matrix<T, Eigen::Dynamic, 1>& src, double** dst)
{
    checkGPU( gpuMalloc( (void**)dst, src.size()*sizeof(T) ) );
    checkGPU( gpuMemcpy( *dst, src.data(), src.size()*sizeof(T), gpuMemcpyHostToDevice) );
}
#endif
#endif /* ENABLE_GPU_SOLVER */


