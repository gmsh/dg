/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#pragma once

#include <vector>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
#include <silo.h>
#pragma clang diagnostic pop

class silo
{
    std::vector<size_t>     node_tag2ofs;
    std::vector<double>     node_coords_x;
    std::vector<double>     node_coords_y;
    std::vector<double>     node_coords_z;
    std::vector<size_t>     nodeTags;
    std::vector<int>        nodelist;
    int                     num_cells;

    DBfile *                m_siloDb;

    int     defvar_seq;

    void    gmsh_get_nodes(void);
    void    gmsh_get_elements(void);

public:
    silo();
    ~silo();

    void    import_mesh_from_gmsh();
    bool    create_db(const std::string&);
    bool    write_mesh(double time = -1.0, int cycle = 0);
    bool    write_zonal_variable(const std::string&, const std::vector<double>&);
    bool    write_zonal_variable(const std::string&, const std::vector<float>&);
    bool    write_scalar_expression(const std::string& name, const std::string& expression);
    bool    write_vector_expression(const std::string& name, const std::string& expression);
    size_t  num_nodes(void) const;
    size_t  node_tag_offset(size_t tag) const;
    void    close_db();


    bool write_nodal_variable(const std::string& name, const std::vector<double>& data);
};
