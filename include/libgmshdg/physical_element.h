/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#pragma once

#include "gmsh.h"
#include "common/point.hpp"
#include "common/eigen.h"
#include "common/types.h"

#ifdef USE_MPI
#include "common/mpi_helpers.h"
#endif /* USE_MPI */

/* This is a key for a basis function. Did this
 * class to protect the code from the GMSH api,
 * which is apparently changing */
class bf_key
{
    int         wtf_int;    // No idea of what this integer represents
    size_t      wtf_uint;   // This is some kind of bf numbering

public:
    bf_key() = default;
    bf_key(const bf_key&) = default;
    bf_key(bf_key&&) = default;
    bf_key(int pi, size_t pu)
        : wtf_int(pi), wtf_uint(pu)
    {}

    bf_key& operator=(const bf_key&) = default;

    bool operator==(const bf_key& other) const
    {
        return (wtf_int == other.wtf_int) and (wtf_uint == other.wtf_uint);
    }

    bool operator<(const bf_key& other) const
    {
        return (wtf_int < other.wtf_int) or
               ( (wtf_int == other.wtf_int) and (wtf_uint < other.wtf_uint) );
    }

    friend std::ostream& operator<<(std::ostream& os, const bf_key&);
};

inline std::ostream& operator<<(std::ostream& os, const bf_key& bk)
{
    os << "{" << bk.wtf_int << ", " << bk.wtf_uint << "}";
    return os;
}

class physical_element
{
    size_t          m_original_position;    /* Position in GMSH ordering (relative to entity) */
    int             m_dimension;            /* Geometric dimension */
    int             m_orientation;          /* GMSH orientation */
    int             m_parent_entity_tag;    /* Tag of the parent entity */
    int             m_gmsh_elemtype;        /* GMSH element type */
    size_t          m_element_tag;          /* Tag of the element */
    vec_size_t      m_node_tags;            /* Tags of the element nodes */
    vec_double      m_determinants;         /* SIGNED determinants in the quadrature points */
    vec_mat3d       m_jacobians;            /* Jacobians in the quadrature points */
    vec_quadpt_3d   m_phys_quadpoints;      /* Quadrature points in the physical element */
    point_3d        m_barycenter;           /* Element barycenter */
    double          m_measure;              /* Element measure */
    int             m_geometric_order;
    int             m_approximation_order;
    std::vector<bf_key>     m_bf_keys;

public:
    physical_element();

    size_t          original_position(void) const;
    int             dimension(void) const;
    int             orientation(void) const;
    int             parent_entity_tag(void) const;
    int             element_tag(void) const;
    int             element_type(void) const;
    vec_size_t      node_tags(void) const;

    const std::vector<bf_key> bf_keys(void) const { return m_bf_keys; }

    double          determinant(void) const;
    double          determinant(size_t) const;
    vec_double      determinants(void) const;

    mat3d           jacobian(void) const;
    mat3d           jacobian(size_t) const;
    vec_mat3d       jacobians(void) const;

    vec_quadpt_3d   integration_points(void) const;
    point_3d        barycenter(void) const;
    double          measure(void) const;

#ifdef USE_MPI
    void            mpi_send(int dst, MPI_Comm comm);
    void            mpi_recv(int src, MPI_Comm comm);
#endif /* USE_MPI */

    /* This class is not intended to be constructed
     * by the user but by the appropriate factory */
    friend class physical_elements_factory;
};

class physical_elements_factory
{
    int         dim;
    int         tag;
    int         elemType;
    int         geom_order;
    int         approx_order;

    std::vector<size_t>     cellTags;
    std::vector<size_t>     cellNodesTags;
    std::vector<double>     cellIps;
    std::vector<double>     cellIws;
    std::vector<double>     cellPpts;
    std::vector<double>     cellJacs;
    std::vector<double>     cellDets;
    std::vector<int>        cellOrientations;
    std::vector<double>     barycenters;
#ifdef USE_INITIAL_4_8_4_API
    gmsh::vectorpair        keypairs;
#else
    std::vector<int>        tagKeys;
    std::vector<size_t>     entityKeys;
#endif
    int                     keys_per_elem;

public:
    physical_elements_factory(const entity_params& ep);

    std::vector<physical_element> get_elements();
};


class element_key
{
    int                     m_dim;
    int                     m_elemType;
    std::array<size_t, 8>   m_key_data{};

public:
    element_key();
    element_key(const element_key&) = default;
    element_key(element_key&&) = default;
    element_key(const physical_element&);
    
    element_key& operator=(const element_key&) = default;

    bool operator<(const element_key&) const;
    bool operator==(const element_key&) const;

    friend class element_key_factory;
    friend std::ostream& operator<<(std::ostream& os, const element_key& ek);
};

std::ostream& operator<<(std::ostream& os, const element_key& ek);

class element_key_factory
{
    std::vector<element_key> ekeys;

public:
    element_key_factory(int dim, int tag, int etype);

    std::vector<element_key> get_keys(void) const;
};









