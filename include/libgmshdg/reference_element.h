/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#pragma once

#ifdef USE_MPI
#include "common/mpi_helpers.h" 
#endif /* USE_MPI */

#include "common/eigen.h"
#include "common/types.h"

class reference_element
{
    int                 m_approx_order;
    int                 m_geometric_order;
    int                 m_dimension;
    int                 m_orientation;
    int                 m_gmsh_elem_type;
    int                 m_parent_entity_tag;
    size_t              m_num_bf;
    vecxd               m_basis_functions;
    matxd               m_basis_gradients;
    matxd               m_mass_matrix;
    matxd               m_mass_matrices;
    vec_quadpt_3d       m_quadpoints;

public:
    reference_element();

    size_t          num_basis_functions(void) const;
    size_t          num_integration_points(void) const;

    vec_quadpt_3d   integration_points(void) const;

    vecxd           basis_functions(size_t iQp) const;
    matxd           basis_gradients(size_t iQp) const;

    vecxd           basis_functions(const point_3d&) const;

    matxd           mass_matrix(void) const;
    matxd           mass_matrix(size_t iQp) const;
    matxd           mass_matrices(void) const;

#ifdef USE_MPI
    void            mpi_send(int dst, MPI_Comm comm);
    void            mpi_recv(int src, MPI_Comm comm);
#endif /* USE_MPI */

    /* This class is not intended to be constructed
     * by the user but by the appropriate factory */
    friend class reference_elements_factory;
};

class reference_elements_factory
{
    int         dim;
    int         tag;
    int         elemType;
    int         geom_order;
    int         approx_order;

public:
    reference_elements_factory(const entity_params& ep);

    std::vector<reference_element> get_elements();
};

