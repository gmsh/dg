/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#pragma once

#include <chrono>

#ifdef HAVE_CUDA_RT
#include <cuda_runtime.h>
#endif

class timecounter
{
    std::chrono::time_point<std::chrono::steady_clock>    begin, end;

public:
    timecounter()
    {}

    void tic()
    {
        begin = std::chrono::steady_clock::now();
    }

    double toc()
    {
        end = std::chrono::steady_clock::now();
        std::chrono::duration<double> diff = end-begin;
        return diff.count();
    }
};

#ifdef HAVE_CUDA_RT
class timecounter_cuda
{
    cudaEvent_t start;
    cudaEvent_t stop;

public:
    timecounter_cuda()
    {
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
    }

    ~timecounter_cuda()
    {
        cudaEventDestroy(start);
        cudaEventDestroy(stop);
    }

    void tic()
    {
        cudaEventRecord(start, 0);
    }

    double toc()
    {
        cudaEventRecord(stop, 0);

        float elapsed;
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&elapsed, start, stop);
        return elapsed/1000.0;
    }
};
#endif
