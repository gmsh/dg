/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#include <iostream>

#include "libgmshdg/gmsh_io.h"
#include "libgmshdg/kernels_cpu.h"
#include "libgmshdg/silo_output.hpp"
#include "maxwell/maxwell_interface.h"
#include "maxwell/maxwell_common.h"
#include "timecounter.h"

namespace maxwell {

/**************************************************************************
 * Solver initialization
 */
void
init_matparams(const model& mod, solver_state& state,
    const parameter_loader& mpl)
{
    state.matparams.num_dofs = mod.num_dofs();
    state.matparams.num_fluxes = mod.num_fluxes();
    state.matparams.inv_epsilon = vecxd::Zero( mod.num_dofs() );
    state.matparams.inv_mu = vecxd::Zero( mod.num_dofs() );
    state.matparams.sigma = vecxd::Zero( mod.num_dofs() );
    state.matparams.sigma_over_epsilon = vecxd::Zero( mod.num_dofs() );

    for (auto& e : mod)
    {
        auto etag = e.material_tag();
        for (size_t iT = 0; iT < e.num_cells(); iT++)
        {
            auto& pe = e.cell(iT);
            auto& re = e.cell_refelem(pe);
            auto bar = pe.barycenter();
            auto epsilon = mpl.epsilon(etag, bar);
            auto mu = mpl.mu(etag, bar);
            auto sigma = mpl.sigma(etag, bar);
            auto ofs = e.cell_model_dof_offset(iT);

            for (size_t iD = 0; iD < re.num_basis_functions(); iD++)
            {
                state.matparams.inv_epsilon(ofs+iD) = 1./epsilon;
                state.matparams.inv_mu(ofs+iD) = 1./mu;
                state.matparams.sigma(ofs+iD) = sigma;
                state.matparams.sigma_over_epsilon(ofs+iD) = sigma/epsilon;
            }
        }
    }

    init_lax_milgram(mod, mpl, state.matparams.aE, state.matparams.bE,
        state.matparams.aH, state.matparams.bH);
    
    init_boundary_conditions(mod, mpl, state.matparams.bc_coeffs);
}

void
init_from_model(const model& mod, solver_state& state)
{
    state.emf_curr.resize( mod.num_dofs() );
    state.emf_next.resize( mod.num_dofs() );
    state.dx.resize( mod.num_dofs() );
    state.dy.resize( mod.num_dofs() );
    state.dz.resize( mod.num_dofs() );
    
    state.k1.resize( mod.num_dofs() );
    state.k2.resize( mod.num_dofs() );
    state.k3.resize( mod.num_dofs() );
    state.k4.resize( mod.num_dofs() );
    state.tmp.resize( mod.num_dofs() );

    state.jumps.resize( mod.num_fluxes() );
    state.fluxes.resize( mod.num_fluxes() );

    state.Jx_src = vecxd::Zero( mod.num_dofs() );
    state.Jy_src = vecxd::Zero( mod.num_dofs() );
    state.Jz_src = vecxd::Zero( mod.num_dofs() );
    state.bndsrcs.resize( mod.num_fluxes() );

    for (auto& e : mod)
    {
        entity_data_cpu ed;
        e.populate_entity_data(ed, mod);
        state.eds.push_back( std::move(ed) );
    }

#ifdef USE_MPI
    auto& cds = mod.comm_descriptors();

    size_t dofs = 0;
    for (auto& [tag, cd] : cds)
        dofs = std::max(cd.dof_mapping.size(), dofs);

    state.ipc.resize(dofs);
#endif /* USE_MPI */

    state.curr_time = 0.0;
    state.curr_timestep = 0;
}


/**************************************************************************
 * Computational kernels: curls
 */
static void
compute_curls(solver_state& state, const field& curr, field& next)
{
    for (const auto& ed : state.eds)
    {
        compute_field_derivatives(ed, curr.Ex, state.dx.Ex, state.dy.Ex, state.dz.Ex);
        compute_field_derivatives(ed, curr.Ey, state.dx.Ey, state.dy.Ey, state.dz.Ey);
        compute_field_derivatives(ed, curr.Ez, state.dx.Ez, state.dy.Ez, state.dz.Ez);
        compute_field_derivatives(ed, curr.Hx, state.dx.Hx, state.dy.Hx, state.dz.Hx);
        compute_field_derivatives(ed, curr.Hy, state.dx.Hy, state.dy.Hy, state.dz.Hy);
        compute_field_derivatives(ed, curr.Hz, state.dx.Hz, state.dy.Hz, state.dz.Hz);
    }

    next.Ex = state.dy.Hz - state.dz.Hy - state.Jx_src;
    next.Ey = state.dz.Hx - state.dx.Hz - state.Jy_src;
    next.Ez = state.dx.Hy - state.dy.Hx - state.Jz_src;
    next.Hx = state.dz.Ey - state.dy.Ez;
    next.Hy = state.dx.Ez - state.dz.Ex;
    next.Hz = state.dy.Ex - state.dx.Ey;
}

static void
compute_curls_E(solver_state& state, const field& curr, field& next)
{
    for (const auto& ed : state.eds)
    {
        compute_field_derivatives(ed, curr.Ex, state.dx.Ex, state.dy.Ex, state.dz.Ex);
        compute_field_derivatives(ed, curr.Ey, state.dx.Ey, state.dy.Ey, state.dz.Ey);
        compute_field_derivatives(ed, curr.Ez, state.dx.Ez, state.dy.Ez, state.dz.Ez);
    }

    next.Hx = state.dz.Ey - state.dy.Ez;
    next.Hy = state.dx.Ez - state.dz.Ex;
    next.Hz = state.dy.Ex - state.dx.Ey;
}

static void
compute_curls_H(solver_state& state, const field& curr, field& next)
{
    for (const auto& ed : state.eds)
    {
        compute_field_derivatives(ed, curr.Hx, state.dx.Hx, state.dy.Hx, state.dz.Hx);
        compute_field_derivatives(ed, curr.Hy, state.dx.Hy, state.dy.Hy, state.dz.Hy);
        compute_field_derivatives(ed, curr.Hz, state.dx.Hz, state.dy.Hz, state.dz.Hz);
    }

    next.Ex = state.dy.Hz - state.dz.Hy - state.Jx_src;
    next.Ey = state.dz.Hx - state.dx.Hz - state.Jy_src;
    next.Ez = state.dx.Hy - state.dy.Hx - state.Jz_src;
}


/**************************************************************************
 * Computational kernels: jumps
 */
static void
compute_field_jumps(solver_state& state, const field& in)
{
    for (auto& ed : state.eds)
    {
        size_t num_all_fluxes = num_elems_all_orientations(ed) * ed.num_faces_per_elem * ed.num_fluxes;
 
        for (size_t i = 0; i < num_all_fluxes; i++)
        {
            auto lofs = i;
            auto gofs = ed.flux_base + i;
            if (ed.fluxdofs_neigh[lofs] != NOT_PRESENT)
            {
                auto pos_mine  = ed.fluxdofs_mine[lofs];
                auto pos_neigh = ed.fluxdofs_neigh[lofs];
                state.jumps.Ex[gofs] = in.Ex[pos_mine] - in.Ex[pos_neigh];
                state.jumps.Ey[gofs] = in.Ey[pos_mine] - in.Ey[pos_neigh];
                state.jumps.Ez[gofs] = in.Ez[pos_mine] - in.Ez[pos_neigh];
                state.jumps.Hx[gofs] = in.Hx[pos_mine] - in.Hx[pos_neigh];
                state.jumps.Hy[gofs] = in.Hy[pos_mine] - in.Hy[pos_neigh];
                state.jumps.Hz[gofs] = in.Hz[pos_mine] - in.Hz[pos_neigh];
            }
            else
            {
                auto bc = state.matparams.bc_coeffs[gofs];
                auto pos_mine  = ed.fluxdofs_mine[lofs];
                state.jumps.Ex[gofs] = bc * in.Ex[pos_mine];
                state.jumps.Ey[gofs] = bc * in.Ey[pos_mine];
                state.jumps.Ez[gofs] = bc * in.Ez[pos_mine];
                state.jumps.Hx[gofs] = (2.0 - bc) * in.Hx[pos_mine];
                state.jumps.Hy[gofs] = (2.0 - bc) * in.Hy[pos_mine];
                state.jumps.Hz[gofs] = (2.0 - bc) * in.Hz[pos_mine];
            }
        }
    }
}

#ifdef USE_MPI
static void
exchange_boundary_data(const model& mod, solver_state& state)
{
    auto& cds = mod.comm_descriptors();
    for (auto& [tag, cd] : cds)
    {
        if (cd.rank_mine < cd.rank_other)
        {
            for (size_t i = 0; i < cd.dof_mapping.size(); i++)
            {
                auto srci = cd.dof_mapping[i];
                state.ipc.Ex[i] = state.jumps.Ex[srci];
                state.ipc.Ey[i] = state.jumps.Ey[srci];
                state.ipc.Ez[i] = state.jumps.Ez[srci];
                state.ipc.Hx[i] = state.jumps.Hx[srci];
                state.ipc.Hy[i] = state.jumps.Hy[srci];
                state.ipc.Hz[i] = state.jumps.Hz[srci];
            }
            send_field(state.ipc, cd.rank_other, tag, 0, cd.dof_mapping.size(), MPI_COMM_WORLD);
            receive_field(state.ipc, cd.rank_other, tag, 0, cd.dof_mapping.size(), MPI_COMM_WORLD);
            for (size_t i = 0; i < cd.dof_mapping.size(); i++)
            {
                auto dsti = cd.dof_mapping[i];
                state.jumps.Ex[dsti] -= state.ipc.Ex[i];
                state.jumps.Ey[dsti] -= state.ipc.Ey[i];
                state.jumps.Ez[dsti] -= state.ipc.Ez[i];
                state.jumps.Hx[dsti] -= state.ipc.Hx[i];
                state.jumps.Hy[dsti] -= state.ipc.Hy[i];
                state.jumps.Hz[dsti] -= state.ipc.Hz[i];
            }
        }
        else
        {
            receive_field(state.ipc, cd.rank_other, tag, 0, cd.dof_mapping.size(), MPI_COMM_WORLD);
            for (size_t i = 0; i < cd.dof_mapping.size(); i++)
            {
                auto dsti = cd.dof_mapping[i];
                double tmp;
                tmp = state.jumps.Ex[dsti]; state.jumps.Ex[dsti] -= state.ipc.Ex[i]; state.ipc.Ex[i] = tmp;
                tmp = state.jumps.Ey[dsti]; state.jumps.Ey[dsti] -= state.ipc.Ey[i]; state.ipc.Ey[i] = tmp;
                tmp = state.jumps.Ez[dsti]; state.jumps.Ez[dsti] -= state.ipc.Ez[i]; state.ipc.Ez[i] = tmp;
                tmp = state.jumps.Hx[dsti]; state.jumps.Hx[dsti] -= state.ipc.Hx[i]; state.ipc.Hx[i] = tmp;
                tmp = state.jumps.Hy[dsti]; state.jumps.Hy[dsti] -= state.ipc.Hy[i]; state.ipc.Hy[i] = tmp;
                tmp = state.jumps.Hz[dsti]; state.jumps.Hz[dsti] -= state.ipc.Hz[i]; state.ipc.Hz[i] = tmp;
            }
            send_field(state.ipc, cd.rank_other, tag, 0, cd.dof_mapping.size(), MPI_COMM_WORLD);
        }
    }
}
#endif /* USE_MPI */

static void
compute_field_jumps_E(solver_state& state, const field& in)
{
    for (auto& ed : state.eds)
    {
        size_t num_all_fluxes = num_elems_all_orientations(ed) * ed.num_faces_per_elem * ed.num_fluxes;
 
        for (size_t i = 0; i < num_all_fluxes; i++)
        {
            auto lofs = i;
            auto gofs = ed.flux_base + i;
            if (ed.fluxdofs_neigh[lofs] != NOT_PRESENT)
            {
                auto pos_mine  = ed.fluxdofs_mine[lofs];
                auto pos_neigh = ed.fluxdofs_neigh[lofs];
                state.jumps.Ex[gofs] = in.Ex[pos_mine] - in.Ex[pos_neigh];
                state.jumps.Ey[gofs] = in.Ey[pos_mine] - in.Ey[pos_neigh];
                state.jumps.Ez[gofs] = in.Ez[pos_mine] - in.Ez[pos_neigh];
            }
            else
            {
                auto bc = state.matparams.bc_coeffs[gofs];
                auto pos_mine  = ed.fluxdofs_mine[lofs];
                state.jumps.Ex[gofs] = bc * in.Ex[pos_mine];
                state.jumps.Ey[gofs] = bc * in.Ey[pos_mine];
                state.jumps.Ez[gofs] = bc * in.Ez[pos_mine];
            }
        }
    }
}

#ifdef USE_MPI
static void
exchange_boundary_data_E(const model& mod, solver_state& state)
{
    auto& cds = mod.comm_descriptors();
    for (auto& [tag, cd] : cds)
    {
        size_t ndofs = cd.dof_mapping.size();
        if (cd.rank_mine < cd.rank_other)
        {
            for (size_t i = 0; i < ndofs; i++)
            {
                auto srci = cd.dof_mapping[i];
                state.ipc.Ex[i] = state.jumps.Ex[srci];
                state.ipc.Ey[i] = state.jumps.Ey[srci];
                state.ipc.Ez[i] = state.jumps.Ez[srci];
            }
            MPI_Send(state.ipc.Ex.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD);
            MPI_Send(state.ipc.Ey.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD);
            MPI_Send(state.ipc.Ez.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD);

            MPI_Recv(state.ipc.Ex.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD, nullptr);
            MPI_Recv(state.ipc.Ey.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD, nullptr);
            MPI_Recv(state.ipc.Ez.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD, nullptr);
            for (size_t i = 0; i < cd.dof_mapping.size(); i++)
            {
                auto dsti = cd.dof_mapping[i];
                state.jumps.Ex[dsti] -= state.ipc.Ex[i];
                state.jumps.Ey[dsti] -= state.ipc.Ey[i];
                state.jumps.Ez[dsti] -= state.ipc.Ez[i];
            }
        }
        else
        {
            MPI_Recv(state.ipc.Ex.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD, nullptr);
            MPI_Recv(state.ipc.Ey.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD, nullptr);
            MPI_Recv(state.ipc.Ez.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD, nullptr);
            for (size_t i = 0; i < cd.dof_mapping.size(); i++)
            {
                auto dsti = cd.dof_mapping[i];
                double tmp;
                tmp = state.jumps.Ex[dsti]; state.jumps.Ex[dsti] -= state.ipc.Ex[i]; state.ipc.Ex[i] = tmp;
                tmp = state.jumps.Ey[dsti]; state.jumps.Ey[dsti] -= state.ipc.Ey[i]; state.ipc.Ey[i] = tmp;
                tmp = state.jumps.Ez[dsti]; state.jumps.Ez[dsti] -= state.ipc.Ez[i]; state.ipc.Ez[i] = tmp;
            }
            MPI_Send(state.ipc.Ex.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD);
            MPI_Send(state.ipc.Ey.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD);
            MPI_Send(state.ipc.Ez.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD);
        }
    }
}
#endif /* USE_MPI */

static void
compute_field_jumps_H(solver_state& state, const field& in)
{
    for (auto& ed : state.eds)
    {
        size_t num_all_fluxes = num_elems_all_orientations(ed) * ed.num_faces_per_elem * ed.num_fluxes;
 
        for (size_t i = 0; i < num_all_fluxes; i++)
        {
            auto lofs = i;
            auto gofs = ed.flux_base + i;
            if (ed.fluxdofs_neigh[lofs] != NOT_PRESENT)
            {
                auto pos_mine  = ed.fluxdofs_mine[lofs];
                auto pos_neigh = ed.fluxdofs_neigh[lofs];
                state.jumps.Hx[gofs] = in.Hx[pos_mine] - in.Hx[pos_neigh];
                state.jumps.Hy[gofs] = in.Hy[pos_mine] - in.Hy[pos_neigh];
                state.jumps.Hz[gofs] = in.Hz[pos_mine] - in.Hz[pos_neigh];
            }
            else
            {
                auto bc = state.matparams.bc_coeffs[gofs];
                auto pos_mine  = ed.fluxdofs_mine[lofs];
                state.jumps.Hx[gofs] = (2.0 - bc) * in.Hx[pos_mine];
                state.jumps.Hy[gofs] = (2.0 - bc) * in.Hy[pos_mine];
                state.jumps.Hz[gofs] = (2.0 - bc) * in.Hz[pos_mine];
            }
        }
    }
}

#ifdef USE_MPI
static void
exchange_boundary_data_H(const model& mod, solver_state& state)
{
    auto& cds = mod.comm_descriptors();
    for (auto& [tag, cd] : cds)
    {
        size_t ndofs = cd.dof_mapping.size();
        if (cd.rank_mine < cd.rank_other)
        {
            for (size_t i = 0; i < ndofs; i++)
            {
                auto srci = cd.dof_mapping[i];
                state.ipc.Hx[i] = state.jumps.Hx[srci];
                state.ipc.Hy[i] = state.jumps.Hy[srci];
                state.ipc.Hz[i] = state.jumps.Hz[srci];
            }
            MPI_Send(state.ipc.Hx.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD);
            MPI_Send(state.ipc.Hy.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD);
            MPI_Send(state.ipc.Hz.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD);
            
            MPI_Recv(state.ipc.Hx.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD, nullptr);
            MPI_Recv(state.ipc.Hy.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD, nullptr);
            MPI_Recv(state.ipc.Hz.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD, nullptr);
            for (size_t i = 0; i < cd.dof_mapping.size(); i++)
            {
                auto dsti = cd.dof_mapping[i];
                state.jumps.Hx[dsti] -= state.ipc.Hx[i];
                state.jumps.Hy[dsti] -= state.ipc.Hy[i];
                state.jumps.Hz[dsti] -= state.ipc.Hz[i];
            }
        }
        else
        {
            MPI_Recv(state.ipc.Hx.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD, nullptr);
            MPI_Recv(state.ipc.Hy.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD, nullptr);
            MPI_Recv(state.ipc.Hz.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD, nullptr);
            for (size_t i = 0; i < cd.dof_mapping.size(); i++)
            {
                auto dsti = cd.dof_mapping[i];
                double tmp;
                tmp = state.jumps.Hx[dsti]; state.jumps.Hx[dsti] -= state.ipc.Hx[i]; state.ipc.Hx[i] = tmp;
                tmp = state.jumps.Hy[dsti]; state.jumps.Hy[dsti] -= state.ipc.Hy[i]; state.ipc.Hy[i] = tmp;
                tmp = state.jumps.Hz[dsti]; state.jumps.Hz[dsti] -= state.ipc.Hz[i]; state.ipc.Hz[i] = tmp;
            }
            MPI_Send(state.ipc.Hx.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD);
            MPI_Send(state.ipc.Hy.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD);
            MPI_Send(state.ipc.Hz.data(), ndofs, MPI_DOUBLE, cd.rank_other, tag, MPI_COMM_WORLD);
        }
    }
}
#endif /* USE_MPI */

/**************************************************************************
 * Computational kernels: fluxes
 */
static void
compute_fluxes_planar(solver_state& state)
{
    for (auto& ed : state.eds)
    {
        size_t num_all_faces = num_elems_all_orientations(ed) * ed.num_faces_per_elem;

        for (size_t iF = 0; iF < num_all_faces; iF++)
        {
            auto n = ed.normals.row(iF);
            auto nx = n[0];
            auto ny = n[1];
            auto nz = n[2];
            auto face_det = ed.face_determinants(iF);

            for (size_t i = 0; i < ed.num_fluxes; i++)
            {
                auto lofs = iF*ed.num_fluxes + i;
                auto gofs = ed.flux_base + lofs;

                /* Sources are imposed on jumps */
                auto jEx = state.jumps.Ex[gofs] - state.bndsrcs.Ex[gofs];
                auto jEy = state.jumps.Ey[gofs] - state.bndsrcs.Ey[gofs];
                auto jEz = state.jumps.Ez[gofs] - state.bndsrcs.Ez[gofs];
                auto jHx = state.jumps.Hx[gofs] + state.bndsrcs.Hx[gofs];
                auto jHy = state.jumps.Hy[gofs] + state.bndsrcs.Hy[gofs];
                auto jHz = state.jumps.Hz[gofs] + state.bndsrcs.Hz[gofs];

                auto ndotE = nx*jEx + ny*jEy + nz*jEz;
                auto ndotH = nx*jHx + ny*jHy + nz*jHz;

                auto aE = face_det * state.matparams.aE[gofs];
                auto bE = face_det * state.matparams.bE[gofs];
                auto aH = face_det * state.matparams.aH[gofs];
                auto bH = face_det * state.matparams.bH[gofs];

                /* Compute fluxes */
                state.fluxes.Ex[gofs] = aE*(nz*jHy - ny*jHz) + bE*(ndotE*nx - jEx);
                state.fluxes.Ey[gofs] = aE*(nx*jHz - nz*jHx) + bE*(ndotE*ny - jEy);
                state.fluxes.Ez[gofs] = aE*(ny*jHx - nx*jHy) + bE*(ndotE*nz - jEz);
                state.fluxes.Hx[gofs] = aH*(ny*jEz - nz*jEy) + bH*(ndotH*nx - jHx);
                state.fluxes.Hy[gofs] = aH*(nz*jEx - nx*jEz) + bH*(ndotH*ny - jHy);
                state.fluxes.Hz[gofs] = aH*(nx*jEy - ny*jEx) + bH*(ndotH*nz - jHz);
            }
        }
    }
}

static void
compute_fluxes_planar_E(solver_state& state)
{
    for (auto& ed : state.eds)
    {
        size_t num_all_faces = num_elems_all_orientations(ed) * ed.num_faces_per_elem;

        for (size_t iF = 0; iF < num_all_faces; iF++)
        {
            auto n = ed.normals.row(iF);
            auto nx = n[0];
            auto ny = n[1];
            auto nz = n[2];
            auto face_det = ed.face_determinants(iF);

            for (size_t i = 0; i < ed.num_fluxes; i++)
            {
                auto lofs = iF*ed.num_fluxes + i;
                auto gofs = ed.flux_base + lofs;

                /* Sources are imposed on jumps */
                auto jEx = state.jumps.Ex[gofs] - state.bndsrcs.Ex[gofs];
                auto jEy = state.jumps.Ey[gofs] - state.bndsrcs.Ey[gofs];
                auto jEz = state.jumps.Ez[gofs] - state.bndsrcs.Ez[gofs];
                auto jHx = state.jumps.Hx[gofs] + state.bndsrcs.Hx[gofs];
                auto jHy = state.jumps.Hy[gofs] + state.bndsrcs.Hy[gofs];
                auto jHz = state.jumps.Hz[gofs] + state.bndsrcs.Hz[gofs];

                auto ndotE = nx*jEx + ny*jEy + nz*jEz;

                auto aE = face_det * state.matparams.aE[gofs];
                auto bE = face_det * state.matparams.bE[gofs];

                /* Compute fluxes */
                state.fluxes.Ex[gofs] = aE*(nz*jHy - ny*jHz) + bE*(ndotE*nx - jEx);
                state.fluxes.Ey[gofs] = aE*(nx*jHz - nz*jHx) + bE*(ndotE*ny - jEy);
                state.fluxes.Ez[gofs] = aE*(ny*jHx - nx*jHy) + bE*(ndotE*nz - jEz);
            }
        }
    }
}

static void
compute_fluxes_planar_H(solver_state& state)
{
    for (auto& ed : state.eds)
    {
        size_t num_all_faces = num_elems_all_orientations(ed) * ed.num_faces_per_elem;

        for (size_t iF = 0; iF < num_all_faces; iF++)
        {
            auto n = ed.normals.row(iF);
            auto nx = n[0];
            auto ny = n[1];
            auto nz = n[2];
            auto face_det = ed.face_determinants(iF);

            for (size_t i = 0; i < ed.num_fluxes; i++)
            {
                auto lofs = iF*ed.num_fluxes + i;
                auto gofs = ed.flux_base + lofs;

                /* Sources are imposed on jumps */
                auto jEx = state.jumps.Ex[gofs] - state.bndsrcs.Ex[gofs];
                auto jEy = state.jumps.Ey[gofs] - state.bndsrcs.Ey[gofs];
                auto jEz = state.jumps.Ez[gofs] - state.bndsrcs.Ez[gofs];
                auto jHx = state.jumps.Hx[gofs] + state.bndsrcs.Hx[gofs];
                auto jHy = state.jumps.Hy[gofs] + state.bndsrcs.Hy[gofs];
                auto jHz = state.jumps.Hz[gofs] + state.bndsrcs.Hz[gofs];

                auto ndotH = nx*jHx + ny*jHy + nz*jHz;

                auto aH = face_det * state.matparams.aH[gofs];
                auto bH = face_det * state.matparams.bH[gofs];

                /* Compute fluxes */
                state.fluxes.Hx[gofs] = aH*(ny*jEz - nz*jEy) + bH*(ndotH*nx - jHx);
                state.fluxes.Hy[gofs] = aH*(nz*jEx - nx*jEz) + bH*(ndotH*ny - jHy);
                state.fluxes.Hz[gofs] = aH*(nx*jEy - ny*jEx) + bH*(ndotH*nz - jHz);
            }
        }
    }
}

/**************************************************************************
 * Computational kernels: boundary terms
 */
static void
compute_fluxes(const model& mod, solver_state& state, const field& in, field& out)
{
    compute_field_jumps(state, in);

#ifdef USE_MPI
    exchange_boundary_data(mod, state);
#endif /* USE_MPI */

    compute_fluxes_planar(state);
    
    for (auto& ed : state.eds)
    {
        compute_flux_lifting(ed, state.fluxes.Ex, out.Ex);
        compute_flux_lifting(ed, state.fluxes.Ey, out.Ey);
        compute_flux_lifting(ed, state.fluxes.Ez, out.Ez);
        compute_flux_lifting(ed, state.fluxes.Hx, out.Hx);
        compute_flux_lifting(ed, state.fluxes.Hy, out.Hy);
        compute_flux_lifting(ed, state.fluxes.Hz, out.Hz);
    }
}

static void
compute_fluxes_E(const model& mod, solver_state& state, const field& in, field& out)
{
    compute_field_jumps_E(state, in);

#ifdef USE_MPI
    exchange_boundary_data_E(mod, state);
#endif /* USE_MPI */
    
    compute_fluxes_planar_E(state);
    
    for (auto& ed : state.eds)
    {
        compute_flux_lifting(ed, state.fluxes.Hx, out.Hx);
        compute_flux_lifting(ed, state.fluxes.Hy, out.Hy);
        compute_flux_lifting(ed, state.fluxes.Hz, out.Hz);
    }
}

static void
compute_fluxes_H(const model& mod, solver_state& state, const field& in, field& out)
{
    compute_field_jumps_H(state, in);

#ifdef USE_MPI
    exchange_boundary_data_H(mod, state);
#endif /* USE_MPI */

    compute_fluxes_planar_H(state);
    
    for (auto& ed : state.eds)
    {
        compute_flux_lifting(ed, state.fluxes.Ex, out.Ex);
        compute_flux_lifting(ed, state.fluxes.Ey, out.Ey);
        compute_flux_lifting(ed, state.fluxes.Ez, out.Ez);
    }
}

/**************************************************************************
 * Computational kernels: time integration
 */
static void
compute_euler_update(solver_state& state, const field& y, const field& k, double dt, field& out)
{
    for (size_t i = 0; i < out.num_dofs; i++)
    {
        auto CR = 1.0 - dt*state.matparams.sigma_over_epsilon[i];
        auto CC = dt*state.matparams.inv_epsilon[i];
        out.Ex[i] = y.Ex[i]*CR + k.Ex[i]*CC;
        out.Ey[i] = y.Ey[i]*CR + k.Ey[i]*CC;
        out.Ez[i] = y.Ez[i]*CR + k.Ez[i]*CC;
    }
    
    for (size_t i = 0; i < out.num_dofs; i++)
    {
        auto CC = dt*state.matparams.inv_mu[i];
        out.Hx[i] = y.Hx[i] + k.Hx[i]*CC;
        out.Hy[i] = y.Hy[i] + k.Hy[i]*CC;
        out.Hz[i] = y.Hz[i] + k.Hz[i]*CC;
    }
}

static void
compute_rk4_weighted_sum(solver_state& state, const field& in, double dt, field& out)
{
    for (size_t i = 0; i < out.num_dofs; i++)
    {
        auto CR = 1.0 - dt*state.matparams.sigma_over_epsilon[i];
        auto CC = dt*state.matparams.inv_epsilon[i];
        out.Ex[i] = in.Ex[i]*CR + CC*(state.k1.Ex[i] + 2*state.k2.Ex[i] + 2*state.k3.Ex[i] + state.k4.Ex[i])/6;
        out.Ey[i] = in.Ey[i]*CR + CC*(state.k1.Ey[i] + 2*state.k2.Ey[i] + 2*state.k3.Ey[i] + state.k4.Ey[i])/6;
        out.Ez[i] = in.Ez[i]*CR + CC*(state.k1.Ez[i] + 2*state.k2.Ez[i] + 2*state.k3.Ez[i] + state.k4.Ez[i])/6;
    }

    for (size_t i = 0; i < out.num_dofs; i++)
    {
        auto CC = dt*state.matparams.inv_mu[i];
        out.Hx[i] = in.Hx[i] + CC*(state.k1.Hx[i] + 2*state.k2.Hx[i] + 2*state.k3.Hx[i] + state.k4.Hx[i])/6;
        out.Hy[i] = in.Hy[i] + CC*(state.k1.Hy[i] + 2*state.k2.Hy[i] + 2*state.k3.Hy[i] + state.k4.Hy[i])/6;
        out.Hz[i] = in.Hz[i] + CC*(state.k1.Hz[i] + 2*state.k2.Hz[i] + 2*state.k3.Hz[i] + state.k4.Hz[i])/6;
    }
}

/**************************************************************************
 * Computational kernels: H&W DG operator
 */
static void
apply_operator(const model& mod, solver_state& state, const field& curr, field& next)
{
    compute_curls(state, curr, next);
    compute_fluxes(mod, state, curr, next);
}

/**************************************************************************
 * Computational kernels: Leapfrog DG
 */
static void
leapfrog(const model& mod, solver_state& state)
{
    auto dt = state.delta_t;
    compute_curls_H(state, state.emf_curr, state.tmp);
    compute_fluxes_H(mod, state, state.emf_curr, state.tmp);
    for (size_t i = 0; i < state.emf_next.num_dofs; i++)
    {
        auto CRM = 1.0 - 0.5*dt*state.matparams.sigma_over_epsilon[i];
        auto CRP = 1.0 + 0.5*dt*state.matparams.sigma_over_epsilon[i];
        auto CR = CRM/CRP;
        auto CC = dt*state.matparams.inv_epsilon[i]/CRP;
        state.emf_next.Ex[i] = state.emf_curr.Ex[i]*CR + state.tmp.Ex[i]*CC; 
        state.emf_next.Ey[i] = state.emf_curr.Ey[i]*CR + state.tmp.Ey[i]*CC; 
        state.emf_next.Ez[i] = state.emf_curr.Ez[i]*CR + state.tmp.Ez[i]*CC; 
    }

    compute_curls_E(state, state.emf_next, state.tmp);
    compute_fluxes_E(mod, state, state.emf_next, state.tmp);
    for (size_t i = 0; i < state.emf_next.num_dofs; i++)
    {
        auto CC = dt*state.matparams.inv_mu[i];
        state.emf_next.Hx[i] = state.emf_curr.Hx[i] + state.tmp.Hx[i]*CC; 
        state.emf_next.Hy[i] = state.emf_curr.Hy[i] + state.tmp.Hy[i]*CC; 
        state.emf_next.Hz[i] = state.emf_curr.Hz[i] + state.tmp.Hz[i]*CC; 
    }
}

/**************************************************************************
 * Computational kernels: Run a whole timestep
 */
void
timestep(const model& mod, solver_state& state, const parameter_loader&, time_integrator_type ti)
{
    if (ti == time_integrator_type::EULER)
    {
        apply_operator(mod, state, state.emf_curr, state.tmp);
        compute_euler_update(state, state.emf_curr, state.tmp, state.delta_t, state.emf_next);
    }

    if (ti == time_integrator_type::RK4)
    {    
        apply_operator(mod, state, state.emf_curr, state.k1);

        compute_euler_update(state, state.emf_curr, state.k1, 0.5*state.delta_t, state.tmp);
        apply_operator(mod, state, state.tmp, state.k2);

        compute_euler_update(state, state.emf_curr, state.k2, 0.5*state.delta_t, state.tmp);
        apply_operator(mod, state, state.tmp, state.k3);
        
        compute_euler_update(state, state.emf_curr, state.k3, state.delta_t, state.tmp);
        apply_operator(mod, state, state.tmp, state.k4);

        compute_rk4_weighted_sum(state, state.emf_curr, state.delta_t, state.emf_next);
    }

    if (ti == time_integrator_type::LEAPFROG)
    {
        leapfrog(mod, state);
    }

    state.curr_time += state.delta_t;
    state.curr_timestep += 1;
}

/**************************************************************************
 * Sources: volumetric
 */
static void
eval_volume_sources(const model& mod, const parameter_loader& mpl, solver_state& state)
{
    for (auto& e : mod)
    {
        auto etag = e.material_tag();
        if ( not mpl.volume_has_source(etag) )
            continue;

        auto fJ = [&](const point_3d& pt) -> vec3d {
            return mpl.eval_volume_source(etag, pt, state.curr_time);
        };

        e.project(fJ, state.Jx_src, state.Jy_src, state.Jz_src);
    }
}

void
do_sources(const model& mod, solver_state& state, parameter_loader& mpl)
{
    if ( mpl.source_has_changed_state() )
    {
        state.Jx_src = vecxd::Zero( state.Jx_src.size() );
        state.Jy_src = vecxd::Zero( state.Jy_src.size() );
        state.Jz_src = vecxd::Zero( state.Jz_src.size() );
        state.bndsrcs.zero();
        mpl.source_was_cleared();
    }

    if ( mpl.volume_sources_enabled() )
        eval_volume_sources(mod, mpl, state);

    if ( mpl.boundary_sources_enabled() )
        eval_boundary_sources(mod, mpl, state, state.bndsrcs);
    
    if ( mpl.interface_sources_enabled() )
        eval_interface_sources(mod, mpl, state, state.bndsrcs);
}

void
prepare_sources(const model& mod, maxwell::solver_state& state,
    maxwell::parameter_loader& mpl)
{
    do_sources(mod, state, mpl);
}

void
swap(maxwell::solver_state& state, const parameter_loader&)
{
    std::swap(state.emf_curr, state.emf_next);
}

#ifdef USE_MPI
static void
gather_field(const model& mod, maxwell::field& in, maxwell::field& out,
    int root, MPI_Comm comm)
{
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == root)
    {
        auto& all_tags = mod.world_entities_tags();
        for (auto& tag : all_tags)
        {
            if (mod.entity_rank(tag) == root)
                continue;

            auto& e = mod.entity_world_lookup(tag);
            auto base = e.dof_base_world();
            auto len = e.num_dofs();
            //std::cout << "GR: " << tag << " " << len << std::endl;
            receive_field(out, mod.entity_rank(tag), tag, base, len, comm);
        }

        for (auto& e : mod)
        {
            auto in_base = e.dof_base();
            auto len = e.num_dofs();
            auto out_base = e.dof_base_world();
            out.Ex.segment(out_base, len) = in.Ex.segment(in_base, len);
            out.Ey.segment(out_base, len) = in.Ey.segment(in_base, len);
            out.Ez.segment(out_base, len) = in.Ez.segment(in_base, len);
            out.Hx.segment(out_base, len) = in.Hx.segment(in_base, len);
            out.Hy.segment(out_base, len) = in.Hy.segment(in_base, len);
            out.Hz.segment(out_base, len) = in.Hz.segment(in_base, len);
        }
    }
    else
    {
        for (auto& e : mod)
        {
            //std::cout << "GS: " << e.gmsh_tag() << " " << e.num_dofs() << std::endl;
            send_field(in, root, e.gmsh_tag(), e.dof_base(), e.num_dofs(), comm);
        }
    }
}

void
export_fields_to_silo(const model &mod, maxwell::solver_state &state,
                      const maxwell::parameter_loader &mpl, std::string basename)
{
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == 0)
    {
        size_t gdnum = mod.num_dofs_world();
        maxwell::field f;
        f.resize(gdnum);
        gather_field(mod, state.emf_next, f, 0, MPI_COMM_WORLD);

        if (basename == "")
            basename = "timestep";

        std::stringstream ss;
        ss << mpl.sim_name() << "/" << basename << "_" << state.curr_timestep << ".silo";

        silo sdb;
        sdb.create_db(ss.str());
        sdb.import_mesh_from_gmsh();
        sdb.write_mesh(state.curr_time, state.curr_timestep);

        auto E_exportmode = mpl.postpro_fieldExportMode("E");
        export_vector_field(mod, sdb, f.Ex, f.Ey, f.Ez, "E", E_exportmode);

        auto H_exportmode = mpl.postpro_fieldExportMode("H");
        export_vector_field(mod, sdb, f.Hx, f.Hy, f.Hz, "H", H_exportmode);

        auto sigma = [&](int tag, const point_3d& pt) -> double { return mpl.sigma(tag, pt); };
        auto J_exportmode = mpl.postpro_fieldExportMode("J");
        export_vector_field(mod, sdb, f.Ex, f.Ey, f.Ez, sigma, "J", J_exportmode);

        if ( mpl.debug_dumpCellRanks() )
        {
            auto entranks = compute_cell_ranks(mod);
            sdb.write_zonal_variable("cell_ranks", entranks);
        }
    }
    else
    {
        maxwell::field dummy;
        gather_field(mod, state.emf_next, dummy, 0, MPI_COMM_WORLD);
    }
}

#else /* USE_MPI */


void
export_fields_to_silo(const model &mod, maxwell::solver_state &state,
                      const maxwell::parameter_loader &mpl, std::string basename)
{
    if (basename == "")
        basename = "timestep";

    std::stringstream ss;
    ss << mpl.sim_name() << "/" << basename << "_" << state.curr_timestep << ".silo";

    silo sdb;
    sdb.create_db(ss.str());
    sdb.import_mesh_from_gmsh();
    sdb.write_mesh(state.curr_time, state.curr_timestep);   

    auto E_exportmode = mpl.postpro_fieldExportMode("E");
    export_vector_field(mod, sdb, state.emf_next.Ex, state.emf_next.Ey,
                state.emf_next.Ez, "E", E_exportmode);
    
    auto H_exportmode = mpl.postpro_fieldExportMode("H");
    export_vector_field(mod, sdb, state.emf_next.Hx, state.emf_next.Hy,
                state.emf_next.Hz, "H", H_exportmode);

    auto sigma = [&](int tag, const point_3d& pt) -> double { return mpl.sigma(tag, pt); };
    auto J_exportmode = mpl.postpro_fieldExportMode("J");
    export_vector_field(mod, sdb, state.emf_next.Ex, state.emf_next.Ey,
                state.emf_next.Ez, sigma, "J", J_exportmode);
}

#endif /* USE_MPI */

} // namespace maxwell
