/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#include "libgmshdg/gmsh_io.h"
#include "maxwell/maxwell_interface.h"
#include "maxwell/maxwell_common.h"

#define DIRICHLET       2.0
#define ROBIN           1.0
#ifdef USE_MPI
#define INTERPROCESS    1.0 
#endif /* USE_MPI */
#define NEUMANN         0.0

namespace maxwell {

/* Take a model and a parameter loader and return a vector with the
 * coefficients 0, 1 and 2 to select the appropriate boundary condition.
 * See the jump kernels for details about the 0, 1, and 2. */
void
init_boundary_conditions(const model& mod, const parameter_loader& mpl,
    vecxd& bcc)
{
    auto bds = mod.boundary_descriptors();
    bcc = 2*vecxd::Ones( mod.num_fluxes() );

    size_t face_num_base = 0;
    for (auto& e : mod)
    {
        for (size_t iF = 0; iF < e.num_faces(); iF++)
        {
            auto gfnum = face_num_base + iF;
            auto bd = bds[gfnum];
            if (bd.type == face_type::NONE)
                continue;

            double bc_coeff = DIRICHLET;
#ifdef USE_MPI
            if (bd.type == face_type::INTERPROCESS_BOUNDARY)
            {
                bc_coeff = INTERPROCESS;
            }
            else
            {
#endif /* USE_MPI */
                switch ( mpl.boundary_type(bd.material_tag()) )
                {
                    case boundary_condition::UNSPECIFIED:
                    case boundary_condition::PEC:
                    case boundary_condition::E_FIELD:
                        bc_coeff = DIRICHLET;
                        break;

                    case boundary_condition::IMPEDANCE:
                    case boundary_condition::PLANE_WAVE_E:
                    case boundary_condition::PLANE_WAVE_H:
                        bc_coeff = ROBIN;
                        break;

                    case boundary_condition::PMC:
                    case boundary_condition::SURFACE_CURRENT:
                    case boundary_condition::H_FIELD:
                        bc_coeff = NEUMANN;
                        break;
               }
#ifdef USE_MPI
            }
#endif /* USE_MPI */
            auto& pf = e.face(iF);
            auto& rf = e.face_refelem(pf);
            auto num_bf = rf.num_basis_functions();
            for (size_t i = 0; i < num_bf; i++)
                bcc(gfnum*num_bf + i) = bc_coeff;
        }

        face_num_base += e.num_faces();
    }
}

/* Take a model and a parameter loader and return the four vectors with
 * the Lax-Milgram flux coefficients */
void
init_lax_milgram(const model& mod, const parameter_loader& mpl,
    vecxd& paE, vecxd& pbE, vecxd& paH, vecxd& pbH)
{
    paE = vecxd::Zero( mod.num_fluxes() );
    pbE = vecxd::Zero( mod.num_fluxes() );
    paH = vecxd::Zero( mod.num_fluxes() );
    pbH = vecxd::Zero( mod.num_fluxes() );

    auto& conn = mod.connectivity();
#ifdef USE_MPI
    auto& ipconn = mod.ip_connectivity();
#endif /* USE_MPI */
    for (auto& e : mod)
    {
        for (size_t iT = 0; iT < e.num_cells(); iT++)
        {
            auto& pe = e.cell(iT);
            auto bar = pe.barycenter();
            auto tag = e.material_tag();
            auto my_eps = mpl.epsilon(tag, bar);
            auto my_mu = mpl.mu(tag, bar);

            auto my_Z = std::sqrt(my_mu/my_eps);
            auto my_Y = 1./my_Z;

            for (size_t iF = 0; iF < e.num_faces_per_elem(); iF++)
            {
                auto face_num = e.num_faces_per_elem()*iT+iF;
                auto& pf = e.face(face_num);
                auto& rf = e.face_refelem(pf);
                auto fk = element_key(pf);

                auto glob_ofs = e.flux_base() + face_num*rf.num_basis_functions();

                auto [neigh, has_neigh] = conn.neighbour_via(e.number(), iT, fk);
                if (has_neigh)
                {
                    auto& ne_e = mod.entity_at(neigh.iE);
                    auto& ne_pe = ne_e.cell(neigh.iT);
                    auto ne_bar = ne_pe.barycenter();
                    auto ne_tag = ne_e.material_tag();
                    auto ne_eps = mpl.epsilon(ne_tag, ne_bar);
                    auto ne_mu = mpl.mu(ne_tag, ne_bar);

                    auto ne_Z = std::sqrt(ne_mu/ne_eps);
                    auto ne_Y = 1./ne_Z;

                    auto aE = ne_Z/(my_Z + ne_Z);
                    auto bE = 1./(my_Z + ne_Z);
                    auto aH = ne_Y/(my_Y + ne_Y);
                    auto bH = 1./(my_Y + ne_Y);

                    for (size_t iD = 0; iD < rf.num_basis_functions(); iD++)
                    {
                        paE(glob_ofs+iD) = aE;
                        pbE(glob_ofs+iD) = bE;
                        paH(glob_ofs+iD) = aH;
                        pbH(glob_ofs+iD) = bH;
                    }
                    continue;
                }
#ifdef USE_MPI
                auto [ip_neigh, has_ip_neigh] = ipconn.neighbour_via(e.number(), iT, fk);
                if (has_ip_neigh)
                {
                    auto ne_eps = mpl.epsilon(ip_neigh.parent_tag, ip_neigh.bar);
                    auto ne_mu = mpl.mu(ip_neigh.parent_tag, ip_neigh.bar);

                    auto ne_Z = std::sqrt(ne_mu/ne_eps);
                    auto ne_Y = 1./ne_Z;

                    auto aE = ne_Z/(my_Z + ne_Z);
                    auto bE = 1./(my_Z + ne_Z);
                    auto aH = ne_Y/(my_Y + ne_Y);
                    auto bH = 1./(my_Y + ne_Y);

                    for (size_t iD = 0; iD < rf.num_basis_functions(); iD++)
                    {
                        paE(glob_ofs+iD) = aE;
                        pbE(glob_ofs+iD) = bE;
                        paH(glob_ofs+iD) = aH;
                        pbH(glob_ofs+iD) = bH;
                    }
                    continue;
                }
#endif /* USE_MPI */
                for (size_t iD = 0; iD < rf.num_basis_functions(); iD++)
                {
                    paE(glob_ofs+iD) = 0.5;
                    pbE(glob_ofs+iD) = 0.5/my_Z;
                    paH(glob_ofs+iD) = 0.5;
                    pbH(glob_ofs+iD) = 0.5/my_Y;
                }
            }
        }
    }
}

#ifdef USE_MPI
std::vector<double>
compute_cell_ranks(const model& mod)
{
    /* This is only for debug purposes: make a vector mapping from the
     * cell number to the rank of the process to which it is assigned */
    std::vector<double> ret( mod.num_cells_world(), 999999999 );
    for (auto& etag : mod.world_entities_tags())
    {
        auto& e = mod.entity_world_lookup(etag);
        int er = mod.entity_rank(etag);

        for (size_t iT = 0; iT < e.num_cells(); iT++)
        {
            auto gi = e.cell_world_index_by_gmsh(iT);
            ret[gi] = er;
        }
    }
    return ret;
}
#endif /* USE_MPI */

static void
export_vector_field_nodal(const model& mod, silo& sdb, const vecxd& Fx,
    const vecxd& Fy, const vecxd& Fz, const MaterialFunc& mf,
    const std::string& field_name)
{
    std::vector<double> accum_Fx( sdb.num_nodes(), 0.0 );
    std::vector<double> accum_Fy( sdb.num_nodes(), 0.0 );
    std::vector<double> accum_Fz( sdb.num_nodes(), 0.0 );
    std::vector<size_t> count( sdb.num_nodes(), 0 );

#ifdef USE_MPI
    for (auto& etag : mod.world_entities_tags())
    {
        auto& e = mod.entity_world_lookup(etag);
#else /* USE_MPI */
    for (auto& e : mod)
    {
#endif /* USE_MPI */
        for (size_t iT = 0; iT < e.num_cells(); iT++)
        {
            auto& pe = e.cell(iT);
            auto cgofs = e.cell_world_dof_offset(iT);
            auto nodetags = pe.node_tags();
            auto mp = mf(e.material_tag(), pe.barycenter());
            for (size_t iD = 0; iD < 4; iD++)
            {
                size_t tag = nodetags[iD];
                size_t svofs = sdb.node_tag_offset(tag);
                accum_Fx[svofs] += mp*Fx[cgofs+iD]; 
                accum_Fy[svofs] += mp*Fy[cgofs+iD]; 
                accum_Fz[svofs] += mp*Fz[cgofs+iD];
                count[svofs]++; 
            }
        }
    }    

    for (size_t i = 0; i < sdb.num_nodes(); i++)
    {
        accum_Fx[i] /= count[i];
        accum_Fy[i] /= count[i];
        accum_Fz[i] /= count[i];
    }

    std::string Fxname = field_name + "x";
    std::string Fyname = field_name + "y";
    std::string Fzname = field_name + "z";
    std::stringstream expression;
    expression << "{" << Fxname << ", " << Fyname << ", " << Fzname << "}";

    sdb.write_nodal_variable(Fxname.c_str(), accum_Fx);
    sdb.write_nodal_variable(Fyname.c_str(), accum_Fy);
    sdb.write_nodal_variable(Fzname.c_str(), accum_Fz);
    sdb.write_vector_expression(field_name.c_str(), expression.str().c_str());  
}

static void
export_vector_field_zonal(const model& mod, silo& sdb, const vecxd& Fx,
    const vecxd& Fy, const vecxd& Fz, const MaterialFunc& mf,
    const std::string& field_name)
{
    std::vector<double> out_Fx( mod.num_cells_world() );
    std::vector<double> out_Fy( mod.num_cells_world() );
    std::vector<double> out_Fz( mod.num_cells_world() );

#ifdef USE_MPI
    for (auto& etag : mod.world_entities_tags())
    {
        auto& e = mod.entity_world_lookup(etag);
#else /* USE_MPI */
    for (auto& e : mod)
    {
#endif /* USE_MPI */
        for (size_t iT = 0; iT < e.num_cells(); iT++)
        {
            auto& pe = e.cell(iT);
            auto& re = e.cell_refelem(pe);
            vecxd phi_bar = re.basis_functions({1./3., 1./3., 1./3.});
            auto mp = mf(e.material_tag(), pe.barycenter());
            auto num_bf = re.num_basis_functions();
            auto ofs = e.cell_world_dof_offset(iT);
            auto gi = e.cell_world_index_by_gmsh(iT);
            out_Fx[gi] = mp*Fx.segment(ofs, num_bf).dot(phi_bar);
            out_Fy[gi] = mp*Fy.segment(ofs, num_bf).dot(phi_bar);
            out_Fz[gi] = mp*Fz.segment(ofs, num_bf).dot(phi_bar);
        }
    }

    std::string Fxname = field_name + "x";
    std::string Fyname = field_name + "y";
    std::string Fzname = field_name + "z";
    std::stringstream expression;
    expression << "{" << Fxname << ", " << Fyname << ", " << Fzname << "}";

    sdb.write_zonal_variable(Fxname.c_str(), out_Fx);
    sdb.write_zonal_variable(Fyname.c_str(), out_Fy);
    sdb.write_zonal_variable(Fzname.c_str(), out_Fz);
    sdb.write_vector_expression(field_name.c_str(), expression.str().c_str());  
}

void
export_vector_field(const model& mod, silo& sdb, const vecxd& Fx, const vecxd& Fy,
    const vecxd& Fz, const std::string& field_name, field_export_mode fem)
{
    auto mf = [](int, const point_3d&) -> double { return 1.0; };

    switch(fem)
    {
        case field_export_mode::NONE:
            break;

        case field_export_mode::NODAL:
            export_vector_field_nodal(mod, sdb, Fx, Fy, Fz, mf, field_name);
            break;
        
        case field_export_mode::ZONAL:
            export_vector_field_zonal(mod, sdb, Fx, Fy, Fz, mf, field_name);
            break;
    }
}

void
export_vector_field(const model& mod, silo& sdb, const vecxd& Fx, const vecxd& Fy,
    const vecxd& Fz, const MaterialFunc& mf, const std::string& field_name,
    field_export_mode fem)
{
    switch(fem)
    {
        case field_export_mode::NONE:
            break;

        case field_export_mode::NODAL:
            export_vector_field_nodal(mod, sdb, Fx, Fy, Fz, mf, field_name);
            break;
        
        case field_export_mode::ZONAL:
            export_vector_field_zonal(mod, sdb, Fx, Fy, Fz, mf, field_name);
            break;
    }
}

} // namespace maxwell
