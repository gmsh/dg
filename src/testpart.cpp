#include <iostream>

#include "gmsh.h"

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        std::cout << "Specify .geo" << std::endl;
        return 1;
    }

    gmsh::initialize();
    gmsh::open(argv[1]);

    gmsh::vectorpair gmsh_entities;

    gmsh::model::getEntities(gmsh_entities, 2);

    for (auto [dim, tag] : gmsh_entities)
    {
        //std::vector<int> elemTypes;
        //gmm::getElementTypes(elemTypes, dim, tag);

        std::string etype;
        gmsh::model::getType(dim, tag, etype);
        std::cout << etype << ", tag: " << tag << std::endl;

        int pdim, ptag;
        gmsh::model::getParent(dim, tag, pdim, ptag);
        std::cout << "  Parent: " << ptag << std::endl;

        std::vector<int> parts;
        gmsh::model::getPartitions(dim, tag, parts);
        std::cout << "  Partitions: ";
        for (const auto& part : parts)
            std::cout << part << " ";
        std::cout << std::endl;
    }

    gmsh::finalize();

    return 0;
}