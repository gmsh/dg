/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#include <iostream>
#include <sstream>
#include <fstream>
#include <cassert>

#include "libgmshdg/gmsh_io.h"
#include "libgmshdg/entity.h"

#ifdef USE_MPI
#include "common/mpi_helpers.h"
#endif /* USE_MPI */

#include "sgr.hpp"
using namespace sgr;

std::string
quadrature_name(int order)
{
    std::stringstream ss;
    ss << "Gauss" << order;
    return ss.str();
}

std::string
basis_func_name(int order)
{
    (void) order;
    std::stringstream ss;
    //ss << "Lagrange";
    ss << "H1Legendre" << order;
    return ss.str();
}

std::string
basis_grad_name(int order)
{
    (void) order;
    std::stringstream ss;
    //ss << "GradLagrange";
    ss << "GradH1Legendre" << order;
    return ss.str();
}

model::model()
    : geometric_order(1), approximation_order(1)
#ifdef USE_MPI
    , num_partitions(1)
#endif /* USE_MPI */
{}

model::model(int pgo, int pao)
    : geometric_order(pgo), approximation_order(pao)
#ifdef USE_MPI
    , num_partitions(1)
#endif /* USE_MPI */
{
    assert(geometric_order > 0);
    assert(approximation_order > 0);
}

model::~model()
{
    //gmsh::clear();
}


void
model::make_boundary_to_faces_map(void)
{
#ifdef USE_MPI
    ASSERT_MPI_RANK_0;
#endif /* USE_MPI */

    gvp_t gmsh_entities;

    /* Create a table mapping the tag of a boundary to the list of
     * its face keys. This must happen before import_gmsh_entities()
     * because otherwise you get spurious 2D entities (and this in
     * turn because the constructor of entity calls addDiscreteEntity()
     * to get all the element faces) .*/
    gm::getEntities(gmsh_entities, DIMENSION(2));
    for (auto [dim, tag] : gmsh_entities)
    {
#ifdef USE_MPI
        if (num_partitions > 1)
        {
            /* If the model is partitioned and entity has no parents,
             * it means that there are no elements in the entity on which
             * we will compute. Skip it. */
            int pdim, ptag;
            gmsh::model::getParent(dim, tag, pdim, ptag);

            if (ptag == -1)
                continue;

            /* This stores which partitions share a boundary. It will be
             * used to determine who communicates with who. */
            std::vector<int> parts;
            gm::getPartitions(dim, tag, parts);
            if (parts.size() == 2)
                comm_map[tag] = parts;

            if (pdim == dim)
                surface_to_parent_map[tag] = ptag;
        }
#endif /* USE_MPI */

        std::vector<int> elemTypes;
        gmm::getElementTypes(elemTypes, dim, tag);
        assert(elemTypes.size() == 1);

        /* Get all the face keys for a given boundary. */
        for (auto& elemType : elemTypes)
        {
            element_key_factory ekf(DIMENSION(2), tag, elemType);
            boundary_map[tag] = ekf.get_keys();
        }
    }

#ifdef USE_MPI
    //for (auto& sp : surface_to_parent_map)
    //    std::cout << sp.first << " -> " << sp.second << std::endl;

    assert( IFF(comm_map.size() == 0, num_partitions == 1) );
#endif /* USE_MPI */
}

#ifdef USE_MPI
void
model::make_partition_to_entities_map()
{
    ASSERT_MPI_RANK_0;

    gvp_t gmsh_entities;

    gm::getEntities(gmsh_entities, DIMENSION(3));
    for (auto [dim, tag] : gmsh_entities)
    {
        if (num_partitions > 1)
        {
            /* If the model is partitioned and entity has no parents,
             * it means that there are no elements in the entity on which
             * we will compute. Skip it. */
            int pdim, ptag;
            gmsh::model::getParent(dim, tag, pdim, ptag);

            if (ptag == -1)
                continue;

            std::vector<int> parts;
            gm::getPartitions(dim, tag, parts);
            assert(parts.size() == 1);
            partition_map[ parts[0] ].push_back(tag);
            partition_inverse_map[tag] = parts[0];
        }
    }

    for (auto& [pn, tags] : partition_map)
        std::sort(tags.begin(), tags.end()); /* For lookups */
}
#endif /* USE_MPI */

void
model::update_connectivity(const entity& e, size_t entity_index)
{
    for (size_t iT = 0; iT < e.num_cells(); iT++)
    {
        for (size_t iF = 0; iF < e.num_faces_per_elem(); iF++)
        {
            size_t findex = e.num_faces_per_elem() * iT  + iF;
            neighbour_descriptor cd(entity_index, iT, iF);
            element_key fk( e.face(findex) );
            conn.connect( fk, cd );
        }
    }
}

#ifdef USE_MPI
int
model::my_partition(void) const
{
    /* Here we are assuming that GMSH enumerates mesh partitions
     * sequentially and starting from 1. */
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    return rank+1;
}
#endif /* USE_MPI */

void
model::import_gmsh_entities_rank0(void)
{
#ifdef USE_MPI
    ASSERT_MPI_RANK_0;
#endif /* USE_MPI */

    gvp_t gmsh_entities;
    gm::getEntities(gmsh_entities, DIMENSION(3));

    size_t entity_index_model = 0;
    size_t dof_base_model = 0;
    size_t flux_base_model = 0;
    size_t index_base_model = 0;
    size_t entity_index_world = 0;
    size_t dof_base_world = 0;
    size_t flux_base_world = 0;
    size_t index_base_world = 0;

    for (auto [dim, tag] : gmsh_entities)
    {
#ifdef USE_MPI
        if (num_partitions > 1)
        {
            /* If the model is partitioned and entity has no parents,
             * it means that there are no elements in the entity on which
             * we will compute. Skip it. */
            int pdim, ptag;
            gmsh::model::getParent(dim, tag, pdim, ptag);

            if (ptag == -1)
                continue;
        }
#endif /* USE_MPI */
        std::vector<int> elemTypes;
        gmm::getElementTypes(elemTypes, dim, tag);
        
        assert(elemTypes.size() == 1);
        if (elemTypes.size() != 1)
            throw std::invalid_argument("Only one element type per entity is allowed");

        for (auto& elemType : elemTypes)
        {
            entity e({.dim = dim, .tag = tag, .etype = elemType,
                      .gorder = geometric_order, .aorder = approximation_order});
            
            e.sort_by_orientation();

#ifdef USE_MPI
            if (num_partitions > 1)
            {
                /* If there are multiple partitions, decide to which one this
                 * entity belongs to. If the partition is remote, send entity
                 * to the remote process. The entity is saved also locally
                 * to have all the data for postprocessing later. */
                auto mp = my_partition();
                auto tag_partition = partition_inverse_map.at(tag);
                bool entity_is_remote = ( tag_partition != mp );
            
                if (entity_is_remote)
                {
                    /* Update world-offsets */
                    e.base_world(dof_base_world, flux_base_world, index_base_world);
                    //e.number(entity_index_world);
                    dof_base_world += e.num_dofs();
                    flux_base_world += e.num_fluxes();
                    index_base_world += e.num_cells();

                    assert(tag_partition > 0);
                    int rank = tag_partition - 1;
#ifdef ENABLE_DEBUG_PRINT
                    std::cout << "Sending entity " << tag << " to " << rank << ", ";
                    std::cout << e.num_dofs() << " DoFs" << std::endl;
#endif /* ENABLE_DEBUG_PRINT */
                    /* Send entity to the appropriate process */
                    e.mpi_send(rank, MPI_COMM_WORLD);
                    /* Save locally the entity */
                    remote_entities[tag_partition].push_back( std::move(e) );
                    all_entities_tags.push_back(tag);
                    entity_index_world++;
                    continue;
                }
            }
#ifdef ENABLE_DEBUG_PRINT
            std::cout << "Keeping entity " << tag << std::endl;
#endif /* ENABLE_DEBUG_PRINT */
#endif /* USE_MPI */
            for (size_t i = 0; i < e.num_cells(); i++)
            {
                const auto& pe = e.cell(i);
                auto eitor = etag_to_entity_offset.find( pe.element_tag() );
                if ( eitor != etag_to_entity_offset.end() )
                    throw std::logic_error("Duplicate tag");

                etag_to_entity_offset[ pe.element_tag() ] = std::make_pair(entity_index_model, i);
            }

            /* Update model-offsets */
            e.base_model(dof_base_model, flux_base_model, index_base_model);
            e.number(entity_index_model);
            dof_base_model += e.num_dofs();
            flux_base_model += e.num_fluxes();
            index_base_model += e.num_cells();

            /* Update world-offsets */
            e.base_world(dof_base_world, flux_base_world, index_base_world);
            //e.number(entity_index_world);
            dof_base_world += e.num_dofs();
            flux_base_world += e.num_fluxes();
            index_base_world += e.num_cells();

            update_connectivity(e, entity_index_model);
            entities.push_back( std::move(e) );
#ifdef USE_MPI
            all_entities_tags.push_back(tag);
#endif /* USE_MPI */
            entity_index_model++;
            entity_index_world++;
        }
    }

#ifdef USE_MPI
    /* Compute DOF offset in the remote entities saved locally. */
    assert( IMPLIES(num_partitions == 1, remote_entities.size() == 0) );
    for (auto& re : remote_entities)
    {
        entity_index_model = 0;
        dof_base_model = 0;
        flux_base_model = 0;
        index_base_model = 0;
        for (auto& e : re.second)
        {
            e.base_model(dof_base_model, flux_base_model, index_base_model);
            e.number(entity_index_model);
            dof_base_model += e.num_dofs();
            flux_base_model += e.num_fluxes();
            index_base_model += e.num_cells();
            entity_index_model++;
        }
    }
    std::sort(all_entities_tags.begin(), all_entities_tags.end());
#endif
}

#ifdef USE_MPI
void
model::import_gmsh_entities_rankN(void)
{
    int mp = my_partition();

    size_t entity_index = 0;
    size_t dof_base = 0;
    size_t flux_base = 0;
    size_t index_base = 0;

    for (auto& rtag : partition_map.at(mp))
    {
        entity e;
#ifdef ENABLE_DEBUG_PRINT
        std::cout << "Receiving " << rtag << ", rank " << mp-1 << ", ";
#endif /* ENABLE_DEBUG_PRINT */
        (void) rtag;
        e.mpi_recv(0, MPI_COMM_WORLD);
#ifdef ENABLE_DEBUG_PRINT
        std::cout << e.num_dofs() << " DoFs" << std::endl;
#endif /* ENABLE_DEBUG_PRINT */

        for (size_t i = 0; i < e.num_cells(); i++)
        {
            const auto& pe = e.cell(i);
            auto eitor = etag_to_entity_offset.find( pe.element_tag() );
            if ( eitor != etag_to_entity_offset.end() )
                throw std::logic_error("Duplicate tag");

            etag_to_entity_offset[ pe.element_tag() ] = std::make_pair(entity_index, i);
        }

        e.base_model(dof_base, flux_base, index_base);
        e.number(entity_index);
        dof_base += e.num_dofs();
        flux_base += e.num_fluxes();
        index_base += e.num_cells();

        update_connectivity(e, entity_index);

        entities.push_back( std::move(e) );
        entity_index++;
    }
}

bool
model::is_interprocess_boundary(int tag)
{
    if (num_partitions > 1)
        return comm_map.find(tag) != comm_map.end();
    
    return false;
}

#endif /* USE_MPI */

std::vector<model::bfk_t>
model::get_facekey_tag_pairs(void)
{
    /* Make a vector mapping an element_key of a boundary face
     * to its entity tag. Sort it to allow fast lookups. */
    std::vector<bfk_t> bfk;
    for (auto& [tag, keys] : boundary_map)
        for (auto& k : keys)
            bfk.push_back( std::make_pair(k, tag) );

    /* Sort it for lookups */
    auto comp = [](const bfk_t& a, const bfk_t& b) -> bool {
        return a.first < b.first;
    };
    std::sort(bfk.begin(), bfk.end(), comp);

    return bfk;
}

void
model::map_boundaries(void)
{
    std::vector<bfk_t> bfk = get_facekey_tag_pairs();

    for (size_t i = 1; i < bfk.size(); i++)
    {
        if ( bfk[i-1].first == bfk[i].first )
        {
            std::cout << redfg << Bon << "WARNING: " << nofg << __FILE__;
            std::cout << "(" << __LINE__ << "): Face identified by ";
            std::cout << bfk[i].first << " was found on interfaces ";
            std::cout << bfk[i-1].second;
#ifdef USE_MPI
            if ( is_interprocess_boundary(bfk[i-1].second) )
                std::cout << " (IP)";
#endif
            std::cout << " and " << bfk[i].second;
#ifdef USE_MPI
            if ( is_interprocess_boundary(bfk[i].second) )
                std::cout << " (IP)";
            int rank;
            MPI_Comm_rank(MPI_COMM_WORLD, &rank);
            std::cout << " [MPI rank " << rank << "]"; 
#endif
            std::cout << ". " << reset << std::endl;
            /*
#ifdef USE_MPI
            int rank;
            MPI_Comm_rank(MPI_COMM_WORLD, &rank);
            if (rank == 0)
#endif
                gmsh::write("crash.msh");
            throw 42;
            */
        }
    }

    bnd_descriptors.resize( num_faces() );
    size_t fbase = 0;
    /* For each entity */
    for (auto& e : entities)
    {
        /* and for each face */
        for (size_t iF = 0; iF < e.num_faces(); iF++)
        {
            /* get the element key */
            element_key fk(e.face(iF));
            auto lbcomp = [](const bfk_t& a, const element_key& b) -> bool {
                return a.first < b;
            };
            /* and lookup it in the vector we created before. */
            auto itor = std::lower_bound(bfk.begin(), bfk.end(), fk, lbcomp);
            if ( (itor == bfk.end()) or not ((*itor).first == fk) )
                continue;
            
            /* If found */
            boundary_descriptor bd;
            /* determine if it is an interface or a boundary */
            if (conn.num_neighbours(fk) == 1)
            {
#ifdef USE_MPI
                if ( is_interprocess_boundary( (*itor).second ) )
                    bd.type = face_type::INTERPROCESS_BOUNDARY;
                else
#endif /* USE_MPI */
                    bd.type = face_type::BOUNDARY;
            }
            else
            {
                bd.type = face_type::INTERFACE;
            }
            /* and store the gmsh tag in the descriptor. */
            bd.gmsh_entity = (*itor).second;
#ifdef USE_MPI
            if (num_partitions > 1)
            {
                auto sitor = surface_to_parent_map.find( (*itor).second );
                if ( sitor != surface_to_parent_map.end() )
                    bd.parent_entity = surface_to_parent_map.at( (*itor).second );
            }
#endif /* USE_MPI */
            /* Finally, put the descriptor in the global array of faces. */
            bnd_descriptors.at(fbase + iF) = bd;
        }
        fbase += e.num_faces();
    }

#ifdef ENABLE_DEBUG_PRINT
    size_t normal = 0;
    size_t interface = 0;
    size_t boundary = 0;
    size_t ipc_boundary = 0;
    for (auto& bd : bnd_descriptors)
    {
        switch (bd.type)
        {
            case face_type::NONE: normal++; break;
            case face_type::INTERFACE: interface++; break;
            case face_type::BOUNDARY: boundary++; break;
#ifdef USE_MPI
            case face_type::INTERPROCESS_BOUNDARY: ipc_boundary++; break;
#endif /* USE_MPI */
        }
    }

    std::cout << bfk.size() << " " << bnd_descriptors.size() << std::endl;
    std::cout << normal << " " << interface << " " << boundary << " " << ipc_boundary << std::endl;
#endif /* ENABLE_DEBUG_PRINT */
}

#ifdef USE_MPI
std::vector<model::bfk_t>
model::get_ip_facekey_tag_pairs(void)
{
    /* Make a vector mapping an element_key of an interprocess boundary
     * face to its entity tag. Sort it to allow fast lookups. */
    std::vector<bfk_t> bfk;
    for (auto& [tag, keys] : boundary_map)
    {
        if ( not is_interprocess_boundary(tag) )
            continue;
        
        for (auto& k : keys)
            bfk.push_back( std::make_pair(k, tag) );
    }
    /* Sort it for lookups */
    auto comp = [](const bfk_t& a, const bfk_t& b) -> bool {
        return a.first < b.first;
    };
    std::sort(bfk.begin(), bfk.end(), comp);

    return bfk;
}

void
model::map_interprocess_boundaries(void)
{
    std::vector<bfk_t> bfk = get_ip_facekey_tag_pairs();

    auto lbcomp = [](const bfk_t& a, const element_key& b) -> bool {
        return a.first < b;
    };

    auto connect = [&](const entity& e, int part) {
        for (size_t iT = 0; iT < e.num_cells(); iT++)
        {
            auto nfpe = e.num_faces_per_elem();
            for (size_t iF = 0; iF < nfpe; iF++)
            {
                auto fnum = iT*nfpe + iF;
                element_key fk(e.face(fnum));

                auto itor = std::lower_bound(bfk.begin(), bfk.end(), fk, lbcomp);
                if ( (itor == bfk.end()) or not ((*itor).first == fk) )
                    continue;
            
                neighbour_descriptor_interprocess ndi;
                ndi.iE = e.number();
                ndi.iT = iT;
                ndi.via_iF = iF;
                ndi.parent_tag = e.material_tag();
                ndi.tag = e.gmsh_tag();
                ndi.partition = part;
                ndi.bar = e.cell(iT).barycenter();
                ipconn.connect(fk, ndi);
            }
        }
    };

    for (auto& e : entities)
    {
        auto part = my_partition();
        assert(part == 1);
        connect(e, part);
    }

    for (auto& [part, res] : remote_entities)
    {
        assert(part != 1);
        for (auto& e : res)
            connect(e, part);
    }
}

void
model::make_comm_descriptors(void)
{
    /* The idea here is to build a vector (cd.dof_mapping) that tracks
     * all the degrees of freedom on a given interprocess boundary.
     * That vector is subsequently used to build the vectors that will
     * be transmitted to (and received from) the neighbouring partitions
     * to compute the jumps. On both sides of the IPC boundary we need
     * the DOFs in cd.dof_mapping listed in the exact same order. 
     * */
    std::vector<bfk_t> bfk = get_ip_facekey_tag_pairs();

    auto lbcomp = [](const bfk_t& a, const element_key& b) -> bool {
        return a.first < b;
    };

    struct dof_position_info {
        element_key                     fk;
        size_t                          fbs;
        size_t                          ibtag;
        std::vector<size_t>             offsets;
    };

    std::vector<dof_position_info> dpis;

    size_t flux_base = 0;
    /* For each entity */
    for (auto& e : entities)
    {
        /* and for each face */
        for (size_t iF = 0; iF < e.num_faces(); iF++)
        {
            /* get the element key */
            auto& pf = e.face(iF);
            auto& rf = e.face_refelem(pf);
            auto fk = element_key(pf);

            /* and lookup it in the vector we created before. */
            auto itor = std::lower_bound(bfk.begin(), bfk.end(), fk, lbcomp);
            if ( (itor == bfk.end()) or not ((*itor).first == fk) )
                continue;
            auto ibtag = (*itor).second;
            auto fbs = rf.num_basis_functions();

            std::vector< std::pair<bf_key, size_t> >  bf_order(fbs);
            auto bf_keys = pf.bf_keys();
            for (size_t i = 0; i < fbs; i++)
                bf_order[i] = std::make_pair( bf_keys[i], i );
            std::sort(bf_order.begin(), bf_order.end());

            dof_position_info dpi;
            dpi.fk = fk;
            dpi.fbs = fbs;
            for (size_t i = 0; i < fbs; i++)
                dpi.offsets.push_back(flux_base + fbs*iF + bf_order[i].second);
            dpi.ibtag = ibtag;
            dpis.push_back( std::move(dpi) );
        }

        flux_base += e.num_fluxes();
    }

    auto dpicomp = [](const dof_position_info& a, const dof_position_info& b) -> bool {
        return a.fk < b.fk;
    };
    std::sort(dpis.begin(), dpis.end(), dpicomp);
    for (auto& dpi : dpis)
    {
        auto& cd = ipc_boundary_comm_table[dpi.ibtag];
        for (size_t i = 0; i < dpi.fbs; i++)
            cd.dof_mapping.push_back( dpi.offsets[i] );
    }


    for (auto& [tag, cd] : ipc_boundary_comm_table)
    {
        cd.boundary_tag = tag;
        auto parts = comm_map.at(tag);
        assert(parts.size() == 2);
        if (parts[0] == my_partition())
        {
            cd.partition_mine = parts[0];
            cd.partition_other = parts[1];
            cd.rank_mine = parts[0]-1;
            cd.rank_other = parts[1]-1;
        }
        else
        {
            cd.partition_mine = parts[1];
            cd.partition_other = parts[0];
            cd.rank_mine = parts[1]-1;
            cd.rank_other = parts[0]-1;
        }
    }
}

#endif

void
model::populate_from_gmsh_rank0()
{
#ifdef USE_MPI
   ASSERT_MPI_RANK_0;
#endif /* USE_MPI */

    gmm::setOrder( geometric_order );
    make_boundary_to_faces_map();

#ifdef USE_MPI
    make_partition_to_entities_map();

    /* At this point, the following members are populated: 
     * - geometric_order, approximation_order and num_partitions 
     * - boundary_map
     * - conn_map
     * - partition_map
     * We broadcast them to the other processes
     */
    MPI_Bcast(&geometric_order, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&approximation_order, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&num_partitions, 1, MPI_INT, 0, MPI_COMM_WORLD);

    priv_MPI_Bcast(boundary_map, 0, MPI_COMM_WORLD);
    priv_MPI_Bcast(comm_map, 0, MPI_COMM_WORLD);
    priv_MPI_Bcast(partition_map, 0, MPI_COMM_WORLD);
    priv_MPI_Bcast(surface_to_parent_map, 0, MPI_COMM_WORLD);

    /*
    for (auto& [tag, vec] : partition_map)
    {
        std::cout << tag << ": " << vec.size() << " -> ";
        for (auto& v : vec)
            std::cout << v << " ";
        std::cout << std::endl;
    }
    */

#endif /* USE_MPI */

    entities.clear();
    import_gmsh_entities_rank0();
    map_boundaries(); /* This must happen after import_gmsh_entities(). */
    
#ifdef USE_MPI
    map_interprocess_boundaries();
    ipconn.mpi_bcast(0, MPI_COMM_WORLD);
    make_comm_descriptors();
#endif /* USE_MPI */
}

#ifdef USE_MPI
void
model::populate_from_gmsh_rankN()
{
    MPI_Bcast(&geometric_order, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&approximation_order, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&num_partitions, 1, MPI_INT, 0, MPI_COMM_WORLD);

    priv_MPI_Bcast(boundary_map, 0, MPI_COMM_WORLD);
    priv_MPI_Bcast(comm_map, 0, MPI_COMM_WORLD);
    priv_MPI_Bcast(partition_map, 0, MPI_COMM_WORLD);
    priv_MPI_Bcast(surface_to_parent_map, 0, MPI_COMM_WORLD);

    /*
    for (auto& [tag, vec] : partition_map)
    {
        std::cout << tag << ": " << vec.size() << " -> ";
        for (auto& v : vec)
            std::cout << v << " ";
        std::cout << std::endl;
    }
    */
    entities.clear();
    import_gmsh_entities_rankN();
    map_boundaries();
    ipconn.mpi_bcast(0, MPI_COMM_WORLD);
    make_comm_descriptors();
}
#endif /* USE_MPI */

void
model::populate_from_gmsh()
{
#ifdef USE_MPI
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (rank == 0)
        populate_from_gmsh_rank0();
    else
        populate_from_gmsh_rankN();
#else /* USE_MPI */
    populate_from_gmsh_rank0();
#endif /* USE_MPI */
}

void
model::build()
{
    //make_boundary_to_faces_map();
    populate_from_gmsh_rank0();
}

#ifdef USE_MPI
void
model::partition_mesh(int parts)
{
    gmm::partition(parts);
    num_partitions = parts;
}

entity&
model::entity_world_lookup(int tag)
{
    /* Look for tag between local entities */
    for (auto& e : entities)
        if (e.gmsh_tag() == tag)
            return e;

    /* Try between remote entities */
    int partition = partition_inverse_map.at(tag);
    for (auto& e : remote_entities[partition])
        if (e.gmsh_tag() == tag)
            return e;
    
    throw std::invalid_argument("tag not found");
}

const entity&
model::entity_world_lookup(int tag) const
{
    /* Look for tag between local entities */
    for (const auto& e : entities)
        if (e.gmsh_tag() == tag)
            return e;

    /* Try between remote entities */
    int partition = partition_inverse_map.at(tag);
    for (const auto& e : remote_entities.at(partition))
        if (e.gmsh_tag() == tag)
            return e;
    
    throw std::invalid_argument("tag not found");
}
#endif /* USE_MPI */

std::vector<entity>::const_iterator
model::begin() const
{
    return entities.begin();
}

std::vector<entity>::const_iterator
model::end() const
{
    return entities.end();
}

std::vector<entity>::iterator
model::begin()
{
    return entities.begin();
}

std::vector<entity>::iterator
model::end()
{
    return entities.end();
}

entity&
model::entity_at(size_t pos)
{
    return entities.at(pos);
}

const entity&
model::entity_at(size_t pos) const
{
    return entities.at(pos);
}

size_t
model::num_dofs(void) const
{
    size_t ret = 0;
    for (const auto& e : entities)
        ret += e.num_dofs();

    return ret;
}

size_t
model::num_fluxes(void) const
{
    size_t ret = 0;
    for (const auto& e : entities)
        ret += e.num_fluxes();

    return ret;
}

size_t
model::num_cells(void) const
{
    size_t ret = 0;
    for (const auto& e : entities)
        ret += e.num_cells();

    return ret;
}

size_t
model::num_faces(void) const
{
    size_t ret = 0;
    for (const auto& e : entities)
        ret += e.num_faces();

    return ret;
}

size_t
model::num_entities(void) const
{
    return entities.size();
}



model::entofs_pair
model::lookup_tag(size_t tag) const
{
    return etag_to_entity_offset.at(tag);
}
