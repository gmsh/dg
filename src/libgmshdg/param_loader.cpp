/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#include <iostream>

#ifdef USE_MPI
#include "common/mpi_helpers.h"
#endif /* USE_MPI */

#include "libgmshdg/param_loader.h"

parameter_loader_base::parameter_loader_base()
    : m_bnd_sources_active(true), m_ifc_sources_active(true),
      m_vol_sources_active(true)
{
    lua.open_libraries(sol::lib::base, sol::lib::math, sol::lib::io);
    init();
}

parameter_loader_base::~parameter_loader_base()
{
    auto on_exit = lua["on_exit"];
    if (on_exit.valid())
        lua["on_exit"]();
}

void
parameter_loader_base::init(void)
{
    lua["const"] = lua.create_table();

    lua["sim"] = lua.create_table();
    lua["sim"]["approx_order"] = 1;
    lua["sim"]["geom_order"] = 1;
    lua["sim"]["use_gpu"] = 0;
    //lua["sim"]["name"];
    //lua["sim"]["dt"];
    //lua["sim"]["timesteps"];
    //lua["sim"]["gmsh_model"];
    lua["sim"]["time_integrator"] = "rk4";

    lua["materials"] = lua.create_table();
    lua["bndconds"] = lua.create_table();
    lua["ifaceconds"] = lua.create_table();
    lua["mesh"] = lua.create_table();

    lua["postpro"] = lua.create_table();
    lua["postpro"]["silo_output_rate"] = 0;
    lua["postpro"]["cycle_print_rate"] = 10;

    lua.set_function("enable_boundary_sources",
                     &parameter_loader_base::enable_boundary_sources,
                     this);

    lua.set_function("enable_interface_sources",
                     &parameter_loader_base::enable_interface_sources,
                     this);

    lua.set_function("enable_volume_sources",
                     &parameter_loader_base::enable_volume_sources,
                     this);

#ifdef USE_MPI
    int comm_rank, comm_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    lua["parallel"] = lua.create_table();
    lua["parallel"]["comm_rank"] = comm_rank;
    lua["parallel"]["comm_size"] = comm_size;
#endif /* USE_MPI */
}

bool
parameter_loader_base::validate_simulation_params(void) const
{
    bool success = true;

    auto sim_name = lua["sim"]["name"];
    if (not sim_name.valid())
    {
        std::cout << "[CONFIG] 'sim.name' not set" << std::endl;
        success = false;
    } 

    auto sim_dt = lua["sim"]["dt"];
    if (not sim_dt.valid())
    {
        std::cout << "[CONFIG] 'sim.dt' not set" << std::endl;
        success = false;
    } 

    auto sim_timesteps = lua["sim"]["timesteps"];
    if (not sim_timesteps.valid())
    {
        std::cout << "[CONFIG] 'sim.timesteps' not set" << std::endl;
        success = false;
    } 

    auto sim_gmshmodel = lua["sim"]["gmsh_model"];
    if (not sim_gmshmodel.valid())
    {
        std::cout << "[CONFIG] 'sim.gmsh_model' not set" << std::endl;
        success = false;
    } 

    return success;
}

bool
parameter_loader_base::load_file(const std::string& fn)
{
    auto script = lua.script_file(fn);
    if (not script.valid())
        return false;

    return validate_simulation_params();
}

int
parameter_loader_base::sim_approxorder(void) const
{
    return lua["sim"]["approx_order"];
}

int
parameter_loader_base::sim_geomorder(void) const
{
    return lua["sim"]["geom_order"];
}

double
parameter_loader_base::sim_dt(void) const
{
    return lua["sim"]["dt"];
}

size_t
parameter_loader_base::sim_timesteps(void) const
{
    return lua["sim"]["timesteps"];
}

std::string
parameter_loader_base::sim_name(void) const
{
    return lua["sim"]["name"];
}

std::string
parameter_loader_base::sim_gmshmodel(void) const
{
    return lua["sim"]["gmsh_model"];
}

bool
parameter_loader_base::sim_usegpu(void) const
{
    int ug = lua["sim"]["use_gpu"];
    return ug != 0;
}

size_t
parameter_loader_base::postpro_cyclePrintRate(void) const
{
    return lua["postpro"]["cycle_print_rate"];
}

size_t
parameter_loader_base::postpro_siloOutputRate(void) const
{
    return lua["postpro"]["silo_output_rate"];
}

std::pair<bool, double>
parameter_loader_base::mesh_scalefactor(void) const
{
    auto msf = lua["mesh"]["scalefactor"];
    if (msf.valid())
        return std::make_pair(true, double(msf));

    return std::make_pair(false, 1.0);
}

void
parameter_loader_base::enable_boundary_sources(bool enable)
{
    m_bnd_sources_active = enable;
    m_source_changed_state = true;
}

void
parameter_loader_base::enable_interface_sources(bool enable)
{
    m_ifc_sources_active = enable;
    m_source_changed_state = true;
}

void
parameter_loader_base::enable_volume_sources(bool enable)
{
    m_vol_sources_active = enable;
    m_source_changed_state = true;
}

bool
parameter_loader_base::boundary_sources_enabled(void) const
{
    return m_bnd_sources_active;
}

bool
parameter_loader_base::interface_sources_enabled(void) const
{
    return m_ifc_sources_active;
}

bool
parameter_loader_base::volume_sources_enabled(void) const
{
    return m_vol_sources_active;
}

void
parameter_loader_base::call_timestep_callback(size_t timestep)
{
    if ( lua["on_timestep"].valid() )
    {
        try {
            lua["on_timestep"](timestep);
        }
        catch (...)
        {
            std::cout << "An error was thrown calling on_timestep()" << std::endl;
        }
    }
}

void
parameter_loader_base::call_initialization_callback()
{
    if ( lua["before_start"].valid() )
    {
        try {
            lua["before_start"]();
        }
        catch (...)
        {
            std::cout << "An error was thrown calling before_start()" << std::endl;
        }
    }
}

void
parameter_loader_base::call_finalization_callback()
{
    if ( lua["on_timestepping_finished"].valid() )
    {
        try {
            lua["on_timestepping_finished"]();
        }
        catch (...)
        {
            std::cout << "An error was thrown calling on_timestepping_finished()" << std::endl;
        }
    }
}

bool
parameter_loader_base::source_has_changed_state(void) const
{
    return m_source_changed_state;
}

void
parameter_loader_base::source_was_cleared(void)
{
    m_source_changed_state = false;
}

time_integrator_type
parameter_loader_base::sim_timeIntegrator(void) const
{
    std::string ti = lua["sim"]["time_integrator"];

    if (ti == "rk4")
        return time_integrator_type::RK4;

    if (ti == "leapfrog")
        return time_integrator_type::LEAPFROG;

    if (ti == "euler")
    {
        std::cout << "[CONFIG] warning: Euler time integrator is only ";
        std::cout << "for testing purposes" << std::endl;
        return time_integrator_type::EULER;
    }

    std::cout << "[CONFIG] warning: invalid time integrator '";
    std::cout << ti << "'" << std::endl;
    return time_integrator_type::RK4;
}

std::string
parameter_loader_base::sim_timeIntegratorName(void) const
{
    std::string ti = lua["sim"]["time_integrator"];
    return ti;
}

