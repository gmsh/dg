/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#include <iostream>
#include <sstream>
#include <cassert>

#include "gmsh.h"
#include "libgmshdg/silo_output.hpp"

#define INVALID_OFS ((size_t) (~0))

silo::silo()
    : m_siloDb(nullptr), defvar_seq(0)
{}

silo::~silo()
{
    close_db();
}

void
silo::gmsh_get_nodes(void)
{
    std::vector<double>     coords;
    std::vector<double>     paraCoords;

    gmsh::model::mesh::getNodes(nodeTags, coords, paraCoords, -1, -1, true, false);

    auto maxtag_pos = std::max_element(nodeTags.begin(), nodeTags.end());
    
    size_t maxtag = 0;
    if (maxtag_pos != nodeTags.end())
        maxtag = (*maxtag_pos)+1;

    node_coords_x.resize( nodeTags.size() );
    node_coords_y.resize( nodeTags.size() );
    node_coords_z.resize( nodeTags.size() );

    for (size_t i = 0; i < nodeTags.size(); i++)
    {
        node_coords_x[i] = coords[3*i+0];
        node_coords_y[i] = coords[3*i+1];
        node_coords_z[i] = coords[3*i+2];
    }

    node_tag2ofs.resize(maxtag, INVALID_OFS);
    for (size_t i = 0; i < nodeTags.size(); i++)
        node_tag2ofs.at( nodeTags[i] ) = i;
}

void
silo::gmsh_get_elements()
{
    gmsh::vectorpair entities;
    num_cells = 0;
    gmsh::model::getEntities(entities, 3);
    for (auto [dim, tag] : entities)
    {
        //if (tag != 4 and tag != 5)
        //    continue;

        std::vector<int> elemTypes;
        gmsh::model::mesh::getElementTypes(elemTypes, dim, tag);
        for (auto& elemType : elemTypes)
        {
            std::vector<size_t> elemTags;
            std::vector<size_t> elemNodeTags;
            gmsh::model::mesh::getElementsByType(elemType, elemTags, elemNodeTags, tag);
            auto nodesPerElem = elemNodeTags.size()/elemTags.size();
            assert( elemTags.size() * nodesPerElem == elemNodeTags.size() );
        
            for (size_t i = 0; i < elemTags.size(); i++)
            {
                auto base = nodesPerElem * i;

                auto node0_tag = elemNodeTags[base + 0];
                assert(node0_tag < node_tag2ofs.size());
                auto node0_ofs = node_tag2ofs[node0_tag];
                assert(node0_ofs != INVALID_OFS);
                nodelist.push_back(node0_ofs + 1);

                auto node1_tag = elemNodeTags[base + 1];
                assert(node1_tag < node_tag2ofs.size());
                auto node1_ofs = node_tag2ofs[node1_tag];
                assert(node1_ofs != INVALID_OFS);
                nodelist.push_back(node1_ofs + 1);

                auto node2_tag = elemNodeTags[base + 2];
                assert(node2_tag < node_tag2ofs.size());
                auto node2_ofs = node_tag2ofs[node2_tag];
                assert(node2_ofs != INVALID_OFS);
                nodelist.push_back(node2_ofs + 1);

                auto node3_tag = elemNodeTags[base + 3];
                assert(node3_tag < node_tag2ofs.size());
                auto node3_ofs = node_tag2ofs[node3_tag];
                assert(node3_ofs != INVALID_OFS);
                nodelist.push_back(node3_ofs + 1);
            }

            num_cells += elemTags.size();
        }
    }

    assert(nodelist.size() == 4*num_cells);
}

void
silo::import_mesh_from_gmsh()
{
    gmsh_get_nodes();
    gmsh_get_elements();        
}

bool
silo::create_db(const std::string& dbname)
{
#ifdef SILO_USE_HDF5
    m_siloDb = DBCreate(dbname.c_str(), DB_CLOBBER, DB_LOCAL, NULL, DB_HDF5);
#else
    m_siloDb = DBCreate(dbname.c_str(), DB_CLOBBER, DB_LOCAL, NULL, DB_PDB);
#endif
    if (m_siloDb)
        return true;

    std::cout << "SILO: Error creating database" << std::endl;
    return false;
}

bool
silo::write_mesh(double time, int cycle)
{
    DBoptlist   *optlist = nullptr;

    if (time >= 0)
    {
        optlist = DBMakeOptlist(2);
        DBAddOption(optlist, DBOPT_CYCLE, &cycle);
        DBAddOption(optlist, DBOPT_DTIME, &time);
    }

    double *coords[] = {node_coords_x.data(), node_coords_y.data(), node_coords_z.data()};

    int lnodelist = nodelist.size();

    int shapetype[] = { DB_ZONETYPE_TET };
    int shapesize[] = {4};
    int shapecounts[] = { num_cells };
    int nshapetypes = 1;
    int nnodes = node_coords_x.size();
    int nzones = num_cells;
    int ndims = 3;

    DBPutZonelist2(m_siloDb, "zonelist", nzones, ndims,
        nodelist.data(), lnodelist, 1, 0, 0, shapetype, shapesize,
        shapecounts, nshapetypes, NULL);

    DBPutUcdmesh(m_siloDb, "mesh", ndims, NULL, coords, nnodes, nzones,
        "zonelist", NULL, DB_DOUBLE, optlist);

    if (optlist)
        DBFreeOptlist(optlist);

    return true;
}

bool
silo::write_zonal_variable(const std::string& name, const std::vector<double>& data)
{
    if (data.size() != num_cells)
    {
        std::cout << "Invalid number of cells" << std::endl;
        return false;
    }

    DBPutUcdvar1(m_siloDb, name.c_str(), "mesh", data.data(), data.size(), NULL,
                    0, DB_DOUBLE, DB_ZONECENT, NULL);
    
    return true;
}

bool
silo::write_nodal_variable(const std::string& name, const std::vector<double>& data)
{
    if (data.size() != num_nodes())
    {
        std::cout << "Invalid number of nodes" << std::endl;
        return false;
    }

    DBPutUcdvar1(m_siloDb, name.c_str(), "mesh", data.data(), data.size(), NULL,
                    0, DB_DOUBLE, DB_NODECENT, NULL);
    
    return true;
}

bool
silo::write_zonal_variable(const std::string& name, const std::vector<float>& data)
{
    if (data.size() != num_cells)
    {
        std::cout << "Invalid number of cells" << std::endl;
        return false;
    }

    DBPutUcdvar1(m_siloDb, name.c_str(), "mesh", data.data(), data.size(), NULL,
                    0, DB_FLOAT, DB_ZONECENT, NULL);
    
    return true;
}

bool
silo::write_scalar_expression(const std::string& name, const std::string& expression)
{
    const char *names[] = { name.c_str() };
    const char *defs[] = { expression.c_str() };
    int type = DB_VARTYPE_SCALAR;
    std::stringstream ss;
    ss << "defvars_" << defvar_seq++;
    DBPutDefvars(m_siloDb, ss.str().c_str(), 1, names, &type, defs, nullptr);
    return true;
}

bool
silo::write_vector_expression(const std::string& name, const std::string& expression)
{
    const char *names[] = { name.c_str() };
    const char *defs[] = { expression.c_str() };
    int type = DB_VARTYPE_VECTOR;
    std::stringstream ss;
    ss << "defvars_" << defvar_seq++;
    DBPutDefvars(m_siloDb, ss.str().c_str(), 1, names, &type, defs, nullptr);
    return true;
}

size_t
silo::num_nodes(void) const
{
    return nodeTags.size();
}

size_t
silo::node_tag_offset(size_t tag) const
{
    assert(tag < node_tag2ofs.size());
    return node_tag2ofs[tag];
}

void
silo::close_db()
{
    if (m_siloDb)
    {
        DBClose(m_siloDb);
        m_siloDb = nullptr;
    }
}
