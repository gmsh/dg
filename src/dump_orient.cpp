#include <iostream>
#include <cassert>

#include "gmsh.h"

namespace g     = gmsh;
namespace go    = gmsh::option;
namespace gm    = gmsh::model;
namespace gmm   = gmsh::model::mesh;
namespace gmo   = gmsh::model::occ;
namespace gmg   = gmsh::model::geo;
namespace gv    = gmsh::view;

using gvp_t = gmsh::vectorpair;

#define SPHERE_RADIUS 0.2
#define DIMENSION(x) x

static void
make_geometry(int order)
{
    gm::add("difftest");

    std::vector<std::pair<int,int>> objects;
    objects.push_back(
        std::make_pair(3, gmo::addBox(0.0, 0.0, 0.0, 1.0, 1.0, 1.0) )
        );
    objects.push_back(
        std::make_pair(3, gmo::addSphere(0.5, 0.5, 0.5, SPHERE_RADIUS) )
        );

    std::vector<std::pair<int, int>> tools;
    gmsh::vectorpair odt;
    std::vector<gmsh::vectorpair> odtm;
    gmo::fragment(objects, tools, odt, odtm);

    gmo::synchronize();

    gvp_t vp;
    gm::getEntities(vp);
    gmm::setSize(vp, 0.5);

    gmm::generate( DIMENSION(3) );
    gmm::setOrder( order );
}

int main(void)
{
    gmsh::initialize();

    int gorder = 2;

    make_geometry(gorder);

    gvp_t gmsh_entities;
    gm::getEntities(gmsh_entities, DIMENSION(3));

    for (auto [dim, tag] : gmsh_entities)
    {
        std::vector<int> elemTypes;
        gmm::getElementTypes(elemTypes, dim, tag);
        
        assert(elemTypes.size() == 1);
        if (elemTypes.size() != 1)
            throw std::invalid_argument("Only one element type per entity is allowed");

        for (auto& elemType : elemTypes)
        {
            std::vector<size_t> elemTags;
            std::vector<size_t> elemNodesTags;
            gmm::getElementsByType(elemType, elemTags, elemNodesTags, tag);

            std::vector<int> orientations;
            gmm::getBasisFunctionsOrientationForElements(elemType, "H1Legendre3",
                orientations, tag);

            std::vector<std::size_t> face_nodes;
            gmm::getElementFaceNodes(elemType, 3, face_nodes, tag); // 3 is for triangles
            int fe_tag = gm::addDiscreteEntity(dim-1);
            int eleType2D = gmsh::model::mesh::getElementType("triangle", gorder);
            gmsh::model::mesh::addElementsByType(fe_tag, eleType2D, {}, face_nodes);

            std::vector<int> orientations2D;
            gmm::getBasisFunctionsOrientationForElements(eleType2D, "H1Legendre3",
                orientations2D, fe_tag);

            std::cout << "3D: " << orientations.size() << std::endl;
            std::cout << "2D: " << orientations2D.size() << std::endl;
            assert(orientations2D.size() == 4*orientations.size());

            using op = std::pair<int, std::array<int, 4>>;
            std::vector<op> ordermap;

            for (size_t i = 0; i < orientations.size(); i++)
            {
                op p;
                p.first = orientations[i];
                std::array<int, 4> or2d;

                for (size_t j = 0; j < 4; j++)
                    or2d[j] = orientations2D[4*i+j];
                
                p.second = or2d;
                ordermap.push_back(p);
            }

            auto comp = [](const op& a, const op& b) -> bool { return a.first < b.first; };

            std::sort(ordermap.begin(), ordermap.end(), comp);

            for (auto& om : ordermap)
            {
                std::cout << om.first << ": ";
                for (auto& o : om.second)
                    std::cout << o << " ";
                std::cout << std::endl;
            }
        }
    }

    gmsh::finalize();
    return 0;
}
