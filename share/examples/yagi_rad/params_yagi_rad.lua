--[[
    Model of a 3-element Yagi antenna radiating at 300 MHz.
    The source is modeled as a volumetric current between the
    two halves of the radiator.
--]]
sim.name = "yagi_rad"               -- simulation name
sim.dt = 1e-13                      -- timestep size
sim.timesteps = 50000               -- num of iterations
sim.gmsh_model = "yagi_rad.geo"     -- gmsh model filename
sim.use_gpu = 0                     -- 0: cpu, 1: gpu
sim.approx_order = 1                -- approximation order
sim.time_integrator = "leapfrog"
postpro.silo_output_rate = 500      -- rate at which to write silo files
postpro.cycle_print_rate = 10       -- console print rate

postpro["H"].silo_mode = "none"
postpro["J"].silo_mode = "zonal"

-- ** Materials **
-- Aluminum
local alu = {2, 3, 6, 8}
for i,v in ipairs(alu) do
    materials[v] = {}
    materials[v].epsilon = 1
    materials[v].mu = 1
    materials[v].sigma = 3.69e7
end

local air = {4, 7}
for i,v in ipairs(air) do
    materials[v] = {}
    materials[v].epsilon = 1
    materials[v].mu = 1
    materials[v].sigma = 0
end

-- ** Boundary conditions **
local Z_bnds = { 17 }
for i,v in ipairs(Z_bnds) do
    bndconds[v] = {}
    bndconds[v].kind = "impedance"
end

local freq = 3e8
function source(tag, x, y, z, t)
    local Ex = 0
    local Ey = 0
    local Ez = math.sin(2*math.pi*t*freq)
    return Ex, Ey, Ez
end

-- Apply the source to subdomain with tag 4
sources[4] = source

enable_boundary_sources(false)
enable_interface_sources(false)

