--[[
    This example simulates a modal converter at 15.5 GHz.
]]--
sim.name = "modeconv"               -- simulation name
sim.dt = 1e-14                      -- timestep size
sim.timesteps = 50000               -- num of iterations
sim.gmsh_model = "modeconv.geo"     -- gmsh model filename
sim.use_gpu = 0                     -- 0: cpu, 1: gpu
sim.approx_order = 2                -- approximation order
sim.time_integrator = "rk4"
postpro.silo_output_rate = 100      -- rate at which to write silo files
postpro.cycle_print_rate = 10       -- console print rate

-- ** Materials **
-- Plastic rods
local rods = {2,3,4,5,6,7,8,9,10,11,12}
for i,v in ipairs(rods) do
    materials[v] = {}
    materials[v].epsilon = 24
    materials[v].mu = 1
    materials[v].sigma = 0
end
-- Air
local air = 111
materials[air] = {}
materials[air].epsilon = 1
materials[air].mu = 1
materials[air].sigma = 0

-- ** Boundary conditions **
-- These conditions are not correct for this model, is a test.
-- waveguide end => impedance
local Z_bnds = {1184}
for i,v in ipairs(Z_bnds) do
    bndconds[v] = {}
    bndconds[v].kind = "impedance"
end

-- waveguide start => plane wave source
local freq = 1.55e10
function boundary_source_1183(tag, x, y, z, t)
    local Ex = 0
    local Ey = 0
    local Ez = math.sin(2*math.pi*t*freq)
    return Ex, Ey, Ez
end

bndconds[1183] = {}
bndconds[1183].kind = "plane_wave_E"
bndconds[1183].source = boundary_source_1183


