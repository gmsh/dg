sim.name = "capacitor"              -- simulation name
sim.dt = 1e-14                      -- timestep size
sim.timesteps = 301                 -- num of iterations
sim.gmsh_model = "capacitor.geo"    -- gmsh model filename
sim.use_gpu = 0                     -- 0: cpu, 1: gpu
sim.approx_order = 1                -- approximation order
sim.time_integrator = "leapfrog"    -- time integration method
postpro.silo_output_rate = 10       -- rate at which to write silo files
postpro.cycle_print_rate = 10       -- console print rate

local diel_epsr = 10    -- Permittivity of the capacitor dielectric

-- Aluminum plates material parameters
local alu = {1, 3}
for i,v in ipairs(alu) do
    materials[v] = {}
    materials[v].epsilon = 1
    materials[v].mu = 1
    materials[v].sigma = 3.69e7
end

-- Dielectric parameters
local diel = { 2 }
for i,v in ipairs(diel) do
    materials[v] = {}
    materials[v].epsilon = diel_epsr
    materials[v].mu = 1
    materials[v].sigma = 0
end

-- Air parameters
local air = { 4 }
for i,v in ipairs(air) do
    materials[v] = {}
    materials[v].epsilon = 1
    materials[v].mu = 1
    materials[v].sigma = 0
end

-- ** Boundary conditions **
local absorbing_bcs = {1}
for i,v in ipairs(absorbing_bcs) do
    bndconds[v] = {}
    bndconds[v].kind = "impedance"
end

function interp(t, t0, t1, y0, y1)
    return (t-t0)*(y1-y0)/(t1-t0) + y0
end

local R   = 0.01    -- Capacitor disk radius (must match capacitor.geo)
local d   = 0.001   -- Dielectric thickness (must match capacitor.geo)
local tV  = 1       -- Target capacitor voltage
local cts = 100     -- Charging current pulse timesteps

local ct  = cts*sim.dt
local eps = const.eps0 * diel_epsr
local A   = math.pi*R*R
local C   = eps*A/d
local J   = tV*eps/(ct*d)
local I   = J*A
local Q   = I*ct

local E   = Q/(eps*A)   -- Expected capacitor field (should equal tV/d)

local do_IO = (not parallel) or (parallel and parallel.comm_rank == 0)

if ( do_IO ) then
    print("\x1b[1mExpected capacitor field: " .. E .. " V/m")
    print("Charge: " .. Q .. " C")
    print("Current density: " .. J .. " A/m^2\x1b[0m")
end

-- Define the shape of charging current density in terms of
-- {time, current density} pairs. Linear interpolation between
-- pairs is used
local current_shape = { {0, J}, {ct, J} }

-- Define the function evaluating the current source
function current_source(tag, x, y, z, t)
    for ii = 1,(#current_shape-1) do
        local t0 = current_shape[ii][1]
        local c0 = current_shape[ii][2]
        local t1 = current_shape[ii+1][1]
        local c1 = current_shape[ii+1][2]
        if (t >= t0 and t < t1) then
            Ez = interp(t, t0, t1, c0, c1)
            return 0, 0, Ez
        end
    end
    return 0, 0, 0
end

-- Apply the current source to entity 2
sources[2] = current_source



