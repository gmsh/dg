R = 0.01;   // disk radius
d = 0.001;  // dielectric thickness
t = 0.0003; // disk thickness

SetFactory("OpenCASCADE");
Mesh.Algorithm3D = 10;

Cylinder(1) = {0, 0, d/2, 0, 0, t, R};
Cylinder(2) = {0, 0, -d/2, 0, 0, d, R};
Cylinder(3) = {0, 0, -d/2-t, 0, 0, t, R};
Sphere(4) = {0, 0, 0, 2*R, -Pi/2, Pi/2, 2*Pi};
Coherence;

Physical Volume(1000) = {1, 3};
Physical Volume(1001) = {2};
Physical Volume(1002) = {4};

Physical Surface(2000) = {1};

MeshSize{ PointsOf{ Volume{2}; } } = 0.0003;
