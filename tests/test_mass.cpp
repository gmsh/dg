/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#include <iostream>
#include <iomanip>

#include <unistd.h>

#include "test.h"
#include "sgr.hpp"
#include "libgmshdg/gmsh_io.h"

using namespace sgr;

static void
make_geometry(int order, double mesh_h)
{
    gm::add("difftest");
    gmo::addBox(0.0, 0.0, 0.0, 1.0, 1.0, 1.0);
    gmo::synchronize();

    gvp_t vp;
    gm::getEntities(vp);
    gmm::setSize(vp, mesh_h);
}

#define OVERINTEGRATE
int test_mass_convergence(int geometric_order, int approximation_order)
{
    std::vector<double> sizes({ 0.32, 0.16, 0.08, 0.04 });
    std::vector<double> errors;

    std::cout << cyanfg << "Testing geometric order " << geometric_order;
    std::cout << ", approximation order = " << approximation_order << nofg;
    std::cout << std::endl;

    auto test_f = [](const point_3d& pt) -> double {
        return std::sin(M_PI*pt.x())*std::sin(M_PI*pt.y())*std::sin(M_PI*pt.z()); 
    };

    for (size_t i = 0; i < sizes.size(); i++)
    {
        double h = sizes[i];
        make_geometry(0,h);
        gmm::generate( DIMENSION(3) );

        model mod(geometric_order, approximation_order);
        mod.build();

        auto& e0 = mod.entity_at(0);
        e0.sort_by_orientation();
        vecxd test_f_e0 = e0.project(test_f);
        double proj_error_e0 = 0.0;
#ifdef OVERINTEGRATE
        auto [rps, pqps] = integrate(e0, 2*approximation_order+1);
        auto num_qp = rps.size();
#endif
        for (size_t iT = 0; iT < e0.num_cells(); iT++)
        {
            auto& pe = e0.cell(iT);
            auto& re = e0.cell_refelem(pe);
            auto num_bf = re.num_basis_functions();

            vecxd pvals = test_f_e0.segment(iT*num_bf, num_bf);
#ifdef OVERINTEGRATE
            for (size_t iQp = 0; iQp < num_qp; iQp++)
            {
                vecxd phi = re.basis_functions( rps[iQp] );
                auto pqpofs = num_qp*iT + iQp; 
                double diff = (pvals.dot(phi) - test_f(pqps[pqpofs].point()));
                proj_error_e0 += std::abs(pqps[pqpofs].weight())*diff*diff;
            }
#else
            auto pqps = pe.integration_points();
            for (size_t iQp = 0; iQp < pqps.size(); iQp++)
            {
                vecxd phi = re.basis_functions( iQp );
                double diff = (pvals.dot(phi) - test_f(pqps[iQp].point()));
                proj_error_e0 += std::abs(pqps[iQp].weight())*diff*diff;
            }
#endif
        }

        errors.push_back( std::sqrt(proj_error_e0) );
    }

    std::cout << Byellowfg << "Errors: " << reset;
    for (auto& error : errors)
        std::cout << error << " ";
    std::cout << std::endl;

    std::cout << Byellowfg << "Rates:  " << reset;
    double rate = 0.0;
    for (size_t i = 1; i < errors.size(); i++)
        std::cout << (rate = std::log2(errors[i-1]/errors[i])) << " ";
    std::cout << std::endl;

    COMPARE_VALUES_ABSOLUTE("Projection", rate, double(approximation_order+1), 0.2);

    return 0;
}

int main(void)
{
    gmsh::initialize();
    gmsh::option::setNumber("General.Terminal", 0);
    gmsh::option::setNumber("Mesh.Algorithm", 1);
    gmsh::option::setNumber("Mesh.Algorithm3D", 1);


    int failed_tests = 0;

    std::cout << Bmagentafg << " *** TESTING: PROJECTIONS ***" << reset << std::endl;
    for (size_t go = 1; go < 5; go++)
    {
        for (size_t ao = go; ao < 5; ao++)
            failed_tests += test_mass_convergence(go, ao);
    }

    return failed_tests;
}
